#include "pidAngle.h"

pidAngle::pidAngle():
  pidX(&InputX, &OutputX, &SetpointX, 0, 0, 0, DIRECT),
  pidY(&InputY, &OutputY, &SetpointY, 0, 0, 0, DIRECT),
  pidZ(&InputZ, &OutputZ, &SetpointZ, 0, 0, 0, DIRECT) {}

void pidAngle::init(uint32_t loopUpdateRate, uint16_t maxAngularSpeedXY, uint16_t maxAngularSpeedZ){
  pidX.SetOutputLimits(-maxAngularSpeedXY, maxAngularSpeedXY);
  pidY.SetOutputLimits(-maxAngularSpeedXY, maxAngularSpeedXY);
  pidZ.SetOutputLimits(-maxAngularSpeedZ, maxAngularSpeedZ);
  pidX.SetSampleTime(1);
  pidY.SetSampleTime(1);
  pidZ.SetSampleTime(1);
  pidX.SetMode(AUTOMATIC);
  pidY.SetMode(AUTOMATIC);
  pidZ.SetMode(AUTOMATIC);
  pidX.SetTunings(X_kp, X_ki, X_kd);
  pidY.SetTunings(Y_kp, Y_ki, Y_kd);
  pidZ.SetTunings(Z_kp, Z_ki, Z_kd);
  pidX.init_DerivativeFilterCompl(derivativeFilterGain);
  pidY.init_DerivativeFilterCompl(derivativeFilterGain);
  pidZ.init_DerivativeFilterCompl(derivativeFilterGain);
}

void pidAngle::updatePIDinput(Vector error){
  InputX = error.x;
  InputY = error.y;
  InputZ = error.z;
}

Vector pidAngle::getPIDoutput(){
  pidX.Compute();
  pidY.Compute();
  pidZ.Compute();
  Vector out(OutputX, OutputY, OutputZ);
  return out;
}

void pidAngle::getPIDcomponents(Vector &pitch, Vector &roll, Vector &yaw){
  pitch.x = pidY.get_proportional();
  pitch.y = pidY.get_integral();
  pitch.z = pidY.get_derivative();

  roll.x = pidX.get_proportional();
  roll.y = pidX.get_integral();
  roll.z = pidX.get_derivative();

  yaw.x = pidZ.get_proportional();
  yaw.y = pidZ.get_integral();
  yaw.z = pidZ.get_derivative();
}

void pidAngle::resetIntegral(){
  pidY.resetIntegral();
  pidX.resetIntegral();
  pidZ.resetIntegral();
}