#pragma once

#include <Arduino.h>
#include "pidClass.h"
#include "mathFunctions.h"

class pidAngle{
    private:
      PID pidX;  
      PID pidY; 
      PID pidZ; 
      double SetpointX = 0, InputX, OutputX;
      double SetpointY = 0, InputY, OutputY;
      double SetpointZ = 0, InputZ, OutputZ;

      float derivativeFilterGain = 1;
      float derivativeFilterCutoff = 40; //Hz
      float X_kp = 200, X_ki = 0, X_kd = 0; // pid coefficients roll
      float Y_kp = 200, Y_ki = 0, Y_kd = 0; // pid coefficients pitch
      float Z_kp = 150, Z_ki = 0, Z_kd = 0; // pid coefficients yaw

    public:
      pidAngle();
      void init(uint32_t loopUpdateRate, uint16_t maxAngularSpeedXY, uint16_t maxAngularSpeedZ);
      void updatePIDinput(Vector error);
      Vector getPIDoutput();
      void tuneUsingSerial();
      void getPIDcomponents(Vector &pitch, Vector &roll, Vector &yaw);
      void resetIntegral();
};