from tkinter.messagebox import showerror
import tkinter as tk
from tkinter import ttk
from typing import Tuple
import numpy as np
import pandas as pd
import pathlib

class RocketPathCreator:
    def __init__(self, root):
        self.root = root
        self.root.title("Rocket Path Creator")

        # Set screen dimensions
        self.SCREEN_WIDTH, self.SCREEN_HEIGHT = 600, 600
        self.CANVAS_WIDTH, self.CANVAS_HEIGHT = 620, 445

        # Canvas to draw points
        self.canvas = tk.Canvas(self.root, width=self.CANVAS_WIDTH, height=self.CANVAS_HEIGHT, bg="white")
        self.canvas.pack(padx=25, pady=25)

        # Frame for buttons and entries grid
        button_frame = tk.Frame(self.root)
        button_frame.pack()

        self.altitude_label = tk.Label(button_frame, text="Altitude")
        self.altitude_label.grid(row=0, column=0, padx=0, pady=10)

        # Altitude entry for single point
        self.altitude_entry = tk.Entry(button_frame, width=50)
        self.altitude_entry.grid(row=1, column=0, padx=10, pady=10)

        self.name_label = tk.Label(button_frame, text="Path name")
        self.name_label.grid(row=0, column=1, padx=0, pady=10)
        
        # Text entry for path name
        self.name_entry = tk.Entry(button_frame, width=50)
        self.name_entry.grid(row=1, column=1, padx=10, pady=10)

        # Button for saving path
        self.save_button = tk.Button(button_frame, text="Save path", width=50, command=self.save_path)
        self.save_button.grid(row=2, column=1, padx=10, pady=10)

        self.points_overview_label = tk.Label(button_frame)
        self.points_overview_label.grid(row=2, column=0, padx=10, pady=10)

        settings_frame = tk.Frame(button_frame)

        self.enclose_path_label = tk.Label(settings_frame, text="Enclose Path")
        self.enclose_path_var = tk.BooleanVar()
        self.enclose_path_checkbox = tk.Checkbutton(settings_frame, variable=self.enclose_path_var, command=self.enclose_path)

        self.round_setpoint_label = tk.Label(settings_frame, text="Round to 0.1m")
        self.round_setpoint_var = tk.BooleanVar()
        self.round_setpoint_checkbox = tk.Checkbutton(settings_frame, variable=self.round_setpoint_var)

        self.enclose_path_checkbox.grid(row=0, column=0, sticky='w')
        self.enclose_path_label.grid(row=0, column=1, sticky='w')
        self.round_setpoint_checkbox.grid(row=0, column=2, sticky='w')
        self.round_setpoint_label.grid(row=0, column=3, sticky='w')

        settings_frame.grid(row=3, column=1)

        # List to store points and corresponding circle sizes
        self.points = []
        self._orinal_canvas_points = []
        self.ovals = []
        self.lines = []

        self.draw_grid()
        
        # Bind left click event to canvas
        self.canvas.bind("<Button-1>", self.save_point)
        self.canvas.bind("<Button-3>", self.remove_last_point)

    def draw_grid(self):
        # Draw horizontal lines
        for y in range(10, self.CANVAS_HEIGHT, 50):
            if (y-10)%100 == 0:
                self.canvas.create_line(0, y, self.CANVAS_WIDTH, y, fill="darkgray")
            else:
                self.canvas.create_line(0, y, self.CANVAS_WIDTH, y, fill="lightgray")
            self.canvas.create_text(10, y, text=2.0-(y-10)*0.005)

        # Draw vertical lines
        for x in range(10, self.CANVAS_WIDTH, 50):
            if (x-10)%100 == 0:
                self.canvas.create_line(x, 0, x, self.CANVAS_HEIGHT, fill="darkgray")
            else:
                self.canvas.create_line(x, 0, x, self.CANVAS_HEIGHT, fill="lightgray")
            self.canvas.create_text(x, self.CANVAS_HEIGHT-10, text=(x-10)*0.005-1.5)
    
    def enclose_path(self):
        if self.enclose_path_var.get():
            self.points.append(self.points[0])
            self.lines.append(self.canvas.create_line(self._orinal_canvas_points[-1], self._orinal_canvas_points[0], width=2, arrow='last'))
        else:
            self.points.pop()
            self.canvas.delete(self.lines.pop())

        self.points_overview_label["text"] = "\n".join(map(str, self.points))

    def save_path(self):
        if not len(self.points):
            showerror("Error", "Path must include at least one setpoint!")
            return
        elif self.name_entry.get().strip() == '':
            showerror("Error", f"Path must have a name, [{self.name_entry.get()}] given")
            return

        checkpoints = np.array(self.points)
        path = pathlib.Path(__file__).parent / "rocket_paths" / f"{self.name_entry.get().strip()}.csv"
        pd.DataFrame(checkpoints).to_csv(path, header=False, index=False)
        self.root.destroy()

    def save_point(self, event):
        def map_range(x, in_min, in_max, out_min, out_max):
            return round((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min, 3)
        
        # Append clicked point to the list
        self._orinal_canvas_points.append([event.x,event.y])
        x = map_range(event.x, 0, 600, -1.5, 1.5)
        y = map_range(event.y, 0, 425, 2.0, -0.25)
        z_text = self.altitude_entry.get()
        z = float(z_text) if z_text != '' else 1.2
        if self.round_setpoint_var.get():
            pos = [round(pos, 1) for pos in [x, y, z]]
        else:
            pos = [x, y, z]
        self.points.append(pos)

        circle_size = 10
        # Draw circle at clicked point
        x0, y0 = event.x - circle_size, event.y - circle_size
        x1, y1 = event.x + circle_size, event.y + circle_size
        self.ovals.append(self.canvas.create_oval(x0, y0, x1, y1, fill="red"))

        if len(self.points) > 1:
            x0, y0 = self._orinal_canvas_points[-2]
            x1, y1 = self._orinal_canvas_points[-1]
            self.lines.append(self.canvas.create_line(x0, y0, x1, y1, width=2, arrow='last'))

        self.points_overview_label["text"] = "\n".join(map(str, self.points))
        
    def remove_last_point(self, event):
        self.points.pop()
        self._orinal_canvas_points.pop()
        self.canvas.delete(self.ovals.pop())
        self.canvas.delete(self.lines.pop())
        self.points_overview_label["text"] = "\n".join(map(str, self.points))

