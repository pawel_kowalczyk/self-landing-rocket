#pragma once

#include <Arduino.h>
#include "sbus.h"
#include "filters.h"

typedef enum {
    Acro,
    Angle,
    Manual,
    Position,
} flightModes;

typedef enum {
    AltitudeHold,
    ManualControl,
} throttleModes;

typedef enum {
    Extended,
    ExtendedWithForce,
    Retracted,
} legsModes;

class RXclass{
    private:
      float filterGain = 0.09;
      float curveExpo = 0.4; // should be between 0 && 1
      float deadband = 0.04;
      float outputMin = -1;
      float outputMax = 1;
      float inputMin = 201;
      float inputMax = 1801;
      bfs::SbusRx sbus_rx;
      bfs::SbusData data;
      bool armed = false;
      bool throttleZero = false;
      Filter filterX, filterY, filterZ;
      
    public:
      RXclass();
      void init();
      void getRX(float (&mapped_channels)[8]);
      float getRX(int i);
      bool isFailsafe();
      bool isArmed();
      bool isThrottleZero();
      bool update();
      float applyCurve(float val);
      float applyDeadband(float val);
      float mapfloat(float x, float in_min, float in_max, float out_min, float out_max);
      flightModes getFlightMode();
      throttleModes getThrottleMode();
      legsModes getLegsMode();
};
