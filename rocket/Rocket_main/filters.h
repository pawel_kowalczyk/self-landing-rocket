#pragma once

#include <Arduino.h>
#include "mathFunctions.h"
#include "arduinoFFT.h"

#define FFT_sample_length 128 // must be power of 2

// #define displayFFTdata   // uncomment to print FFT data, to be used with the python program

typedef enum {
    FILTER_LPF,
    FILTER_NOTCH,
    FILTER_BPF,
} filterType;

class Filter{
    private:
      double x1, x2, y1, y2, a1, a2, b0, b1, b2;

    public:
      double complFilterPrev = 0, complFilterNew, complGain = 0;
      Filter();
      void init_complementaryFilter(float complGain);
      double process_complementaryFilter(double val);
      void init_biquadFilter(filterType type, float filterFreq, uint32_t samplingFreq, float Q);
      void biquadFilterUpdate(filterType type, float filterFreq, uint32_t samplingFreq, float Q);
      double process_biquadFilter(double input);
};

class FFT{
    private:
      int fftUpdateSampleNumber = FFT_sample_length;
      double samplingFrequency = 1000, minFFT_frequency = 60, peakRemoveSamples = 4;
      float FFT_FilterGain = 0.4;
      int minFFT_frequency_num, count, sampleCount;
      bool averageGyroSample = false;
      double prevGyroSample = 0;
      double FFT_queue[FFT_sample_length];
      double vReal[FFT_sample_length];
      double vImag[FFT_sample_length];
      double prevResult[FFT_sample_length];
      double vDeriv[FFT_sample_length];
      ArduinoFFT<double> fft = ArduinoFFT<double>(vReal, vImag, FFT_sample_length, samplingFrequency);

    public:
      FFT();
      void init();
      void compute(double newValue, double &peak1, double &magn1, double &averageMagn);
      void findPeakManual();
};