import multiprocessing
from dataclasses import dataclass
import datetime
import tkinter as tk
from typing import Dict, List, Tuple
import cv2
from basler_camera_set import BaslerCameraSet
from rocket_position_calculation import RocketPositionCalculation as RPC
from rocket_communication import RocketCommunication
from dummy_rocket_communication import DummyRocketCommunication
from dummy_basler_camera_set import DummyBaslerCameraSet
from logger import DataLogger
import numpy as np
from rocket_path_selector import RocketPathSelectorApp
from utilities import RGDBFrame

import cv2
import time
from threading import Thread

@dataclass
class Setpoint:
    """A single setpoint in a path."""
    id: int
    value: Tuple[float, float, float]

class RocketPath:
    """A Rocket path."""
    def __init__(self, path: np.ndarray) -> None:
        self.setpoints = self._load_setpoints(path)
        self.current_idx = 0

    def get_starting_setpoint(self) -> Setpoint:
        return self.setpoints[0]
    
    def get_next_setpoint(self) -> Setpoint:
        if self.current_idx + 1 == len(self.setpoints):
            return None
        
        self.current_idx += 1
        return self.setpoints[self.current_idx]
    
    def _load_setpoints(self, path: np.ndarray) -> List[Setpoint]:
        setpoints = []
        for idx, setpoint in enumerate(path.tolist()):
            setpoints.append(Setpoint(idx, setpoint))
        return setpoints
    
    def reset(self):
        """Sets `current_idx` to 0."""
        self.current_idx = 0

class BaseStationApp:
    def __init__(self, master, camera_angle: float, is_dummy_camera: bool = False, is_dummy_connection: bool = False):

        ############################
        ####### GUI Settings #######
        ############################

        self.master = master
        self.master.title("Control Center")
        
        self.current_position_label = tk.Label(self.master, font=15, text="Input Desired Coordinates", bg="lightgray")
        self.current_position_label.pack(padx=10, pady=5)
        
        # Text field for sending commands
        self.send_entry = tk.Entry(self.master)
        self.send_entry.pack(fill=tk.X, padx=10, pady=5)
        self.placeholder = "Input format (X Y Z), X: <-1.5,1.5>, Y: <0.0,2.0>, Z: <0.0,1.5>"
        self.send_entry.insert(0, self.placeholder)
        self.send_entry.bind("<FocusIn>", self.on_entry_focus_in)
        self.send_entry.bind("<FocusOut>", self.on_entry_focus_out)

        # Button for sending commands
        self.set_setpoint_button = tk.Button(self.master, text="Send Coordinates", command=self.send_coordinates)
        self.set_setpoint_button.pack(padx=10, pady=5)
        
        # Text field for receiving data
        self.text_display = tk.Text(self.master, height=10, width=70)
        self.text_display.pack(fill=tk.X, padx=10, pady=5)
        
        
        # Frame for button grid
        button_frame = tk.Frame(self.master)
        button_frame.pack()
        button_frame.columnconfigure(2, weight=1)


        # Select rocket preset path
        self.select_rocket_path_button = tk.Button(button_frame, text="Select Rocket Path", width=15, command=self.open_rocket_path_selector_window)
        self.select_rocket_path_button.grid(row=0, column=0, padx=5, pady=5)

        # Run preset setpoints
        self.start_selected_path_button = tk.Button(button_frame, text="Start Path", width=15, state="disabled", command=self.start_selected_path)
        self.start_selected_path_button.grid(row=1, column=0, padx=5, pady=5)
        

        # Start data recording
        self.start_recording_button = tk.Button(button_frame, text="Start Recording", width=15, command=self.start_recording)
        self.start_recording_button.grid(row=0, column=1, padx=5, pady=5)
        
        # Stop data recording
        self.stop_recording_button = tk.Button(button_frame, text="Stop Recording", state="disabled", width=15, command=self.stop_recording)
        self.stop_recording_button.grid(row=1, column=1, padx=5, pady=5)


        # Set Home button
        self.set_home_button = tk.Button(button_frame, text="Set Home", width=15, command=self.set_home)
        self.set_home_button.grid(row=1, column=2, padx=5, pady=5)


        # Stop button to cancel setpoint
        self.stop_rocket_button = tk.Button(button_frame, text="Stop", width=15, command=self.send_stop)
        self.stop_rocket_button.grid(row=2, column=2, padx=5, pady=5)


        # Takeoff button        
        self.takeoff_button = tk.Button(button_frame, text="Takeoff", width=15, state="normal", command=self.takeoff)
        self.takeoff_button.grid(row=0, column=3, padx=5, pady=5)
        
        # Land button        
        self.land_button = tk.Button(button_frame, text="Land", width=15, state="disabled", command=self.land)
        self.land_button.grid(row=1, column=3, padx=5, pady=5)
        
        # Return home button        
        self.return_home_button = tk.Button(button_frame, text="Home", width=15, state="disabled", command=self.return_home)
        self.return_home_button.grid(row=0, column=4, padx=5, pady=5)
        
        # Full operation button        
        self.full_operation_home_button = tk.Button(button_frame, text="Full", width=15, state="disabled", command=self.full_operation)
        self.full_operation_home_button.grid(row=1, column=4, padx=5, pady=5)
        
        lb = tk.Label(button_frame, width=15)
        lb.grid(row=0, column=2)

        ###################################
        ###### Base Station Settings ######
        ###################################

        # Select dummy instances for debuging
        self.is_dummy_camera = is_dummy_camera
        self.is_dummy_connection = is_dummy_connection
        self.camera = DummyBaslerCameraSet(camera_angle, is_rand=False) if is_dummy_camera else BaslerCameraSet(camera_angle)
        self.rocket_communication = DummyRocketCommunication(99) if is_dummy_connection else RocketCommunication(3)

        # None-value images from Basler Camera set
        self.none_frame_count = 0

        # Lost rocket counter during one recording
        self.lost_rocket_count = 0

        # Logger settings
        self.rocket_data = []
        self.is_recording = False
        self.recording_start_time = 0.0
        
        self.frames_data: Dict[float, np.ndarray] = {}

        # Selected rocket path and current setpoint 
        self.rocket_path: RocketPath = None
        self.current_setpoint: Tuple[float, float, float] = None
        self.is_running_automatically = False

        # Home position for the rocket
        self.home_coordinates = None

        # Setpoint arrival timer
        self.time_in_setpoint = 0
        self.last_setpoint_time = None
        
        # Current rocket position 
        self.rocket_coordinates = None 

        self.running = False

    @property
    def is_rocket_in_air(self):
        if self.rocket_coordinates:
            return self.rocket_coordinates[2] > 0.2
        else:
            return None 
    
    def setup_rocket_communication(self):
        self.rocket_communication.setup_connection()

    def setup_camera(self):
        self.camera.setup_camera_set()
    
    def setup_opencv_windows(self):
        cv2.namedWindow("Concatenated")
        cv2.moveWindow("Concatenated", 5, 10)

        cv2.namedWindow("Masked")
        cv2.moveWindow("Masked", 1280, 10)
        
        cv2.namedWindow("Masked area")
        cv2.moveWindow("Masked area", 1280, 515)

    def run(self):
        print("Initializing the camera set.")
        self.setup_camera()
        print("Finished initializing the camera set.\n")

        print("Initializing Bluetooth connection.")
        self.setup_rocket_communication()
        print("Finished initializing Bluetooth connection.\n")

        print("Starting processing loop.\n")
        self.running = True  # Enable loops
        self.time_0 = time.time()
        Thread(target=self.operation_loop).start()
    
    def calibrate_camera_angle(self):
        height = 0.15

        rgb, depth = self.camera.get_images()
        while any(frame is None for frame in [rgb, depth]):
            rgb, depth = self.camera.get_images()
            
        top_markers = sorted(RPC.get_rocket_markers(rgb), key=lambda values: values[1])[:2]
        RPC.average_distance_from_markers(top_markers)
        height = (top_markers[0][1] + top_markers[1][1])/2



    def save_frames(self):
        Thread(target=self.logger.save_frames, args=(self.frames_data.copy(), )).start()
        self.frames_data.clear()

    def start_recording(self):
        """Start recording data."""
        self.logger = DataLogger()
        self.is_recording = True
        self.recording_start_time = time.time()
        self.start_recording_button.config(state="disabled")
        self.stop_recording_button.config(state="normal")
        self.display_message(f"Recording started")

    def stop_recording(self):
        """Stop and save recording data and plots."""
        self.is_recording = False
        self.stop_recording_button.config(state="disabled")
        multiprocessing.Process(target=self.logger.save, args=(self.rocket_data.copy(), )).run()
        self.save_frames()
        # Thread(target=self.logger.save, args=(self.rocket_data.copy(), )).start()
        self.rocket_data.clear()

        self.start_recording_button.config(state="normal")
        self.display_message(f"Recording finished")

    def load_rocket_path(self, setpoints: np.ndarray, name: str):
        """Create rocket path based on given setpoints."""
        if self.rocket_path is None and self.is_running_automatically == False:
            self.rocket_path = RocketPath(setpoints)
            self.start_selected_path_button.config(state="normal")
            self.full_operation_home_button.config(state="normal")
            self.display_message(f"Loaded path: [{name}]") 
        else:
            self.display_message("Can't load another path, one already in progress\nPress 'Stop' to terminate it.")
            
    def start_selected_path(self):
        """Start selected rocket path."""
        if self.rocket_path is not None and self.is_running_automatically == False:
            self.is_running_automatically = True
            self.start_selected_path_button.config(state="disabled")
            self.current_setpoint = self.rocket_path.get_starting_setpoint().value
            self.display_message("Starting Path")
            self.display_message(f"Changing to Setpoint: {self.current_setpoint}")
        else:
            self.display_message("Can't start another path, one already in progress\nPress 'Stop' to terminate it.")
            
    def open_rocket_path_selector_window(self):
        """Open a window where you can select rocket's path."""
        self.rocket_path_selector_window = tk.Toplevel(self.master)
        self.rocket_path_selector = RocketPathSelectorApp(self.rocket_path_selector_window, self)

    def set_home(self):
        if self.rocket_coordinates:
            if self.is_rocket_in_air:
                self.display_message(f"Land to set home!")
            else:            
                self.home_coordinates = self.rocket_coordinates
                self.display_message(f"Home set at {self.home_coordinates}")
        else:
            self.display_message("Rocket's position unknown, can't set home.")

    def single_setpoint(self, setpoint: Tuple[float, float, float]):
        # self.load_rocket_path(np.array([setpoint]), "Single Setpoint")
        # self.start_selected_path()
        self.display_message(f"Loaded path: [Single Setpoint]")
        self.is_running_automatically = True
        self.rocket_path = RocketPath(np.array([setpoint]))
        self.current_setpoint = self.rocket_path.get_starting_setpoint().value
        self.display_message("Starting Path")
        self.display_message(f"Changing to Setpoint: {self.current_setpoint}")

    def takeoff(self):
        if not self.is_rocket_in_air:
            self.load_rocket_path(np.array([(*self.rocket_coordinates[:2], 0.8)]), "Takeoff")
            self.start_selected_path()

    def land(self):
        if self.is_rocket_in_air:
            self.load_rocket_path(np.array([self.rocket_coordinates, (*self.rocket_coordinates[:2], 0.0)]), "Landing")
            self.start_selected_path()
            
    def return_home(self):
        if self.home_coordinates:
            self.load_rocket_path(np.array([(*self.home_coordinates[:2], 1.3), (*self.home_coordinates[:2], 0.0)]), "Return Home")
            self.start_selected_path()
        else:
            self.display_message("The 'Home' position was never set.")

    def full_operation(self):
        self.rocket_path = RocketPath(
            np.array(
                [
                    (*self.rocket_coordinates[:2], 0.8),
                    *(setpoint.value for setpoint in self.rocket_path.setpoints),
                    (*self.home_coordinates[:2], 1.3), (*self.home_coordinates[:2], 0.0),
                ]
            )
        )
        self.display_message(f"Loaded path: [Full Operation]")
        self.is_running_automatically = True
        self.start_selected_path_button.config(state="disabled")
        self.current_setpoint = self.rocket_path.get_starting_setpoint().value
        self.display_message("Starting Path")
        self.display_message(f"Changing to Setpoint: {self.current_setpoint}")
        
    def is_setpoint_reached(self) -> bool:
        """Returns `True` if rocket remains within a `20 cm` radius sphere for more than `2 seconds`."""
        if all(abs(coord - setpoint_coord) < 0.20 for coord, setpoint_coord in zip(self.rocket_coordinates, self.current_setpoint)):
            
            if self.last_setpoint_time is None:
                self.last_setpoint_time = time.time()

            current_setpoint_time = time.time()
            self.time_in_setpoint += current_setpoint_time - self.last_setpoint_time
            self.last_setpoint_time = current_setpoint_time
        else:
            self.time_in_setpoint = 0
            self.last_setpoint_time = None

        if self.time_in_setpoint >= 2:
            return True
        else:
            return False

    def send_coordinates(self):
        coords = self.send_entry.get().strip().split(' ')

        if self.is_dummy_connection and coords[0] == 's':
            coords = coords[1:]
            self.rocket_coordinates = [float(coord) for coord in coords]
            self.display_message("Position set (X:{}, Y:{}, Z:{})".format(*coords))
            self.send_entry.delete(0, tk.END)
            return
        
        if coords[0] == 'c':
            self.camera.camera_angle = float(coords[1])
            self.send_entry.delete(0, tk.END)
            self.display_message(f"Camera angle: {self.camera.camera_angle}*")
            return
        
        if self.send_entry.get() == self.placeholder:
            self.display_message(f"ERROR: Empty input")
            return

        if len(coords) != 3:
            self.display_message(f"ERROR: Invalid input format!expected [X Y Z], got {coords}")
            self.send_entry.delete(0, tk.END)
            return
        
        try:        
            x,y,z = [float(coord) for coord in coords] 
        except ValueError:
            self.display_message(f"ERROR: Incorrect input format!\n-expected [float float float]\n-got {coords}")     
            self.send_entry.delete(0, tk.END)

        if (-1.5 < x > 1.5) or (0 < y > 2) or (0 < z > 1.5):
            self.display_message(f"ERROR: Value outside of workspace!\n-expected: [X:<-1.5,1.5>, Y:<0.0,2.0>, Z:<0.0,1.5>]\n-got: [X:{x}, Y:{y}, Z:{z}]")
        else:
            self.single_setpoint((x,y,z))
        
        self.send_entry.delete(0, tk.END)

    def send_stop(self):
        if self.is_running_automatically:
            self.rocket_path = None
            self.current_setpoint = None
            self.is_running_automatically = False
            self.time_in_setpoint = 0

        self.display_message("STOP")

    def operation_loop(self):
        # if self.is_dummy_camera:
        #     self.rocket_coordinates = (1,1,0.15) 
        self.setup_opencv_windows()

        while self.running:
            loop_start_time = time.time()
        
            rgb, depth = self.camera.get_images()
            if any(frame is None for frame in [rgb, depth]):
                self.none_frame_count += 1

            else:  
                if not self.is_dummy_camera:
                    self.rocket_coordinates = RPC.get_rocket_3d_position(rgb, depth, self.camera.get_coefs(), self.camera.camera_angle, mark_positions=True, show_coordinates=False)
                # self.rocket_coordinates = RPC.get_rocket_3d_position(rgb, depth, self.camera.get_coefs(), self.camera.camera_angle, mark_positions=True, show_coordinates=False)

                if self.rocket_coordinates is None:
                    self.rocket_communication.rocket_lost(round(time.time()-self.time_0, 3))
                    
                    if self.is_recording:
                        self.lost_rocket_count += 1
                        sp = self.current_setpoint or (None, None, None)
                        current_time = round(time.time() - self.recording_start_time, 3)
                        self.rocket_data.append([current_time,None,None,None,*sp])

                        self.frames_data.update({current_time: RGDBFrame.create(rgb, depth)})

                    cv2.putText(rgb, "Current Position: UNKNOWN!", (10, 50), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,255,0))

                else:
                    # Set 'home' right after starting the Base Station
                    if self.home_coordinates is None:
                        self.set_home()

                    if self.current_setpoint is None:
                        self.rocket_communication.send_current_coordinates(round(time.time()-self.time_0, 3), self.rocket_coordinates)
                        cv2.putText(rgb, "Current Setpoint: Not Set", (10, 90), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,255,0))
                        if self.is_rocket_in_air:
                            self.takeoff_button.config(state="disabled")
                            self.land_button.config(state="normal")
                            self.return_home_button.config(state="normal")

                        elif self.is_rocket_in_air == False:
                            self.takeoff_button.config(state="normal")
                            self.land_button.config(state="disabled")
                            self.return_home_button.config(state="disabled")

                    else:
                        self.rocket_communication.send_current_and_setpoint(round(time.time()-self.time_0, 3), self.rocket_coordinates, self.current_setpoint)
                        cv2.putText(rgb, "Current Setpoint: (X:{:.3f}, Y:{:.3f}, Z:{:.3f})".format(*self.current_setpoint), (10, 90), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,255,0))
                        if self.is_running_automatically:
                            if self.is_setpoint_reached():
                                next_setpoint = self.rocket_path.get_next_setpoint()
                                if next_setpoint is None:
                                    self.is_running_automatically = False
                                    self.rocket_path = None
                                    self.current_setpoint = None
                                    self.full_operation_home_button.config(state="disabled")
                                    self.display_message(f"Finished Path")

                                else:
                                    self.current_setpoint = next_setpoint.value
                                    self.display_message(f"Changing to Setpoint: {self.current_setpoint}")
                                
                    if self.is_recording:
                        sp = self.current_setpoint or (None, None, None)
                        current_time = round(time.time() - self.recording_start_time, 3)
                        self.rocket_data.append([current_time, *self.rocket_coordinates, *sp])

                        self.frames_data.update({current_time: RGDBFrame.create(rgb, depth)})

                    cv2.putText(rgb, "Current Position: (X:{:.3f}, Y:{:.3f}, Z:{:.3f})".format(*self.rocket_coordinates), (10, 50), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,255,0))

                display_depth = cv2.applyColorMap(cv2.convertScaleAbs(depth, alpha=0.03), cv2.COLORMAP_JET)
                
                cv2.putText(rgb, f"Lost Frames: {self.lost_rocket_count}", (10, 130), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,255,0))

                cv2.imshow("Concatenated", np.concatenate((rgb, display_depth), axis=1))

                if len(self.frames_data) == 10:
                    self.save_frames()

                sleep_time = 0.1
                elapsed = time.time()-loop_start_time
                sleep_time = sleep_time - elapsed
                if sleep_time < 0:
                    sleep_time = 0

                time.sleep(sleep_time)

                if cv2.pollKey() in [ord('q'), 27]:
                    self.running = False
                    time.sleep(1)
                    break

            # print(f"FPS: {int(1/(time.time()-loop_start_time))}")

    def display_message(self, msg):
        msg = f'[{datetime.datetime.now().strftime("%H-%M-%S")}]-{msg}'
        self.text_display.insert(tk.END, f"{msg}\n")
        self.text_display.see("end")

    def on_entry_focus_in(self, event):
        if self.send_entry.get() == self.placeholder:
            self.send_entry.delete(0, tk.END)

    def on_entry_focus_out(self, event):
        if not self.send_entry.get():
            self.send_entry.insert(0, self.placeholder)
            
    def terminate(self):
        print("Terminated")
        self.camera.terminate()
        self.rocket_communication.terminate()
        self.master.destroy()

    def __del__(self):
        self.terminate()