
from typing import List, Optional, Tuple
import cv2
import numpy as np
import time

class DummyBaslerCameraSet:
    def __init__(self, camera_angle: float, is_rand: Optional[bool] = True):
        self.camera_angle = camera_angle
        self.is_rand = is_rand

        if not self.is_rand:
            self.create_image()

    def create_image(self):
        self.rgb = np.zeros((480, 640, 3), dtype=np.uint8)

        cv2.circle(self.rgb, (200, 200), 3, (255,255,255), -1)
        cv2.circle(self.rgb, (280, 200), 3, (255,255,255), -1)
        cv2.circle(self.rgb, (230, 260), 3, (255,255,255), -1)

        self.depth = np.full((480, 640), 25000, dtype=np.uint16)

    def setup_camera_set(self):
        self.running = True

    def get_images(self) -> Tuple[np.ndarray, np.ndarray]:
        if self.is_rand:
            self.rgb = np.random.choice(np.arange(start=0, stop=140, dtype=np.uint8), size=(480, 640, 3), replace=True)
            self.depth = np.random.choice(np.arange(start=0, stop=65535, dtype=np.uint16), size=(480, 640), replace=True)
        time.sleep(0.07)
        return [self.rgb.copy(), self.depth.copy()]

    def get_coefs(self) -> List:
        return [-4.5707015475665500e+01, 1.5447263010977310e+00, -2.5163773518261571e+01]

    def terminate(self):
        self.running = False
        pass

    def __del__(self):
        self.terminate()
