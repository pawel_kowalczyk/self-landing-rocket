import numpy as np
from logger import DataLogger
import pathlib

if __name__ == "__main__":    
    path = "X:\\GitLab\\AIE_Projects\\P4\\self-landing-rocket\\base_station_gui\\results\\result_26-05-2024_17-28-05\\raw_data.csv"
    data = np.loadtxt(path, delimiter=',', skiprows=1)
    # data[(data == -4) | (data == -5)] = None
    # data[(data == -4) | (data == -5)] = None
    # print(data)
    logger = DataLogger(pathlib.Path(__file__).parent / "test_dir")
    logger.save(data)
