import pathlib
from typing import Dict, List, Tuple
import re
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
import csv

class DataLogger:
    def __init__(self, result_folder_path: pathlib.Path = pathlib.Path(__file__).parent / 'results') -> None:
        self.result_folder_path = result_folder_path

        self._prepare_results_folder()
        self._prepare_destination_folder()
        
        self.last_frame_index = 0

    def _prepare_results_folder(self):
        pathlib.Path.mkdir(self.result_folder_path, exist_ok=True)

    def _prepare_destination_folder(self):
        dt_string = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")

        self.current_result_path = self.result_folder_path / f"result_{dt_string}" 
        pathlib.Path.mkdir(self.current_result_path, exist_ok=True)

        self.frames_folder_path = self.current_result_path / "frames"
        pathlib.Path.mkdir(self.frames_folder_path, exist_ok=True)

    def _save_raw_data(self, raw_data):    
        file_path = self.current_result_path / f'raw_data.csv'

        with open(file_path, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow("txyzuvk")
            writer.writerows(raw_data)


    def _save_figures(self, raw_data: np.ndarray):
        data = raw_data
        plt.plot(data[:, 0], data[:, 1])
        plt.plot(data[:, 0], data[:, 4])
        plt.grid()
        plt.gca().legend(('Position','Setpoint'))
        plt.xlabel("Time [s]")
        plt.ylabel("X Position [m]")
        plt.ylim(-2.0, 2.0)
        plt.savefig(self.current_result_path / f"plot_x.svg")
        plt.cla()

        plt.plot(data[:, 0], data[:, 2])
        plt.plot(data[:, 0], data[:, 5])
        plt.grid()
        plt.gca().legend(('Position','Setpoint'))
        plt.xlabel("Time [s]")
        plt.ylabel("Y Position [m]")
        plt.ylim(-0.5, 2.5)
        plt.savefig(self.current_result_path / f"plot_y.svg")
        plt.cla()
        
        plt.plot(data[:, 0], data[:, 3])
        plt.plot(data[:, 0], data[:, 6])
        plt.grid()
        plt.gca().legend(('Position','Setpoint'))
        plt.xlabel("Time [s]")
        plt.ylabel("Z Position [m]")
        plt.ylim(-0.2, 3.0)
        plt.savefig(self.current_result_path / f"plot_z.svg")
        plt.cla()

    def save_frames(self, data: Dict[float, np.ndarray]):
        for time, rgbd in data.items():
            time = str(time).replace(".", "_")
            rgbd.tofile(f"{self.frames_folder_path}/{time}.bin")

    def save(self, data: List[Tuple[float, float, float, float]]):
        data = np.array(data)
        data[(data == -4) | (data == -5) | (data == None)] = np.nan
        self._save_raw_data(data)
        self._save_figures(data)
