#include "telemetry.h"

bool getArgFromString(String s, String sf, float &param);
float vectorDistance(Vector v1, Vector v2);
Vector interpolation3D(Vector v1, Vector v2, float t);
Vector rotateVectorByAngles(Vector vec, Vector angles);

#define ser Serial5

telemetry::telemetry()
{}

void telemetry::init(){
  ser.begin(115200);
  velQueue[0].x = 0; velQueue[0].y = 0; velQueue[0].z = 0;
  posQueue[0].x = 0; posQueue[0].y = 0; posQueue[0].z = 0;
}

bool telemetry::update(){
  String s = "";
  if (ser.available()) {
    s = ser.readStringUntil('\n');
    // Serial.println(s);
    ser.flush();
  }
  processTargetPosition(s);
  return processString(s);
}

bool telemetry::processString(String s){
  if (s == ""){ return false; }
  bool newVal = false, bt = false;
  float tempX = 0, tempY = 0, tempZ = 0, tempT = 0;

  bt = getArgFromString(s, "z", tempZ);
  if (bt && (tempZ > -0.5)){ // check if camera sees rocket
    lastMesTime = millis();
    newVal = true;
  }

  if (newVal){
    positionZ = tempZ * rawFilterGain + prevZ * (1-rawFilterGain);
    prevZ = positionZ;

    if (getArgFromString(s, "x", tempX)){
      positionX = tempX * rawFilterGain + prevX * (1-rawFilterGain);
      prevX = positionX;
    }

    if (getArgFromString(s, "y", tempY)){
      positionY = tempY * rawFilterGain + prevY * (1-rawFilterGain);
      prevY = positionY;
    }

    if (getArgFromString(s, "t", tempT)){
      p0.t = tempT;
    }

    // calculate velocity and acceleration for the current point
    p0.z = positionZ;
    p0.x = positionX;
    p0.y = positionY;
    float dt = p0.t - p1.t;
    p0.vx = (p0.x - p1.x) / dt;
    p0.vy = (p0.y - p1.y) / dt;
    p0.vz = (p0.z - p1.z) / dt;
    p0.ax = (p0.vx - p1.vx) / dt;
    p0.ay = (p0.vy - p1.vy) / dt;
    p0.az = (p0.vz - p1.vz) / dt;
    p0.ax = p0.ax * accFilterGain + prev_p0.ax * (1-accFilterGain);
    p0.ay = p0.ay * accFilterGain + prev_p0.ay * (1-accFilterGain);
    p0.az = p0.az * accFilterGain + prev_p0.az * (1-accFilterGain);
    prev_p0 = p0;
    p1 = p0;

    this->updatePredictionWithCamera();
  }

  return newVal;
}

Vector telemetry::getPredictedPosition(){
  Vector out;
  float dt = (millis() - lastMesTime) / 1000.0f + predictionTime;
  out.x = p0.x + p0.vx * dt + p0.ax * dt * dt * 0.5;
  out.y = p0.y + p0.vy * dt + p0.ay * dt * dt * 0.5;
  out.z = p0.z + p0.vz * dt + p0.az * dt * dt * 0.5;

  out.x = out.x * predFilterGain + prevPredicted.x * (1-predFilterGain);
  out.y = out.y * predFilterGain + prevPredicted.y * (1-predFilterGain);
  out.z = out.z * predFilterGain + prevPredicted.z * (1-predFilterGain);
  prevPredicted.x = out.x;
  prevPredicted.y = out.y;
  prevPredicted.z = out.z;
  return out;
}

void telemetry::updatePredictionWithIMU(Vector acc, Vector angle){
  Vector newPos, newVel, prevPos, prevVel;
  float g = 9.8;

  accRotated = rotateVectorByAngles(acc, angle);
  //switch x and y
  float tt = accRotated.x;
  accRotated.x = accRotated.y;
  accRotated.y = tt;

  //rotate acc vector by angle
  prevPos = posQueue[0];
  prevVel = velQueue[0];

  for (int i=cameraPositionDelay-1; i>0; i--){
    posQueue[i] = posQueue[i-1];
    velQueue[i] = velQueue[i-1];
  }

  if (micros()-prevIntTime>100000) { prevIntTime = micros(); }
  float dt = (micros() - prevIntTime)/1000000.0f;
  prevIntTime = micros();
  newVel.x = prevVel.x + accRotated.x * dt * g;
  newVel.y = prevVel.y + accRotated.y * dt * g;
  newVel.z = prevVel.z + (accRotated.z - 1) * dt * g;
  newPos.x = prevPos.x + newVel.x * dt;
  newPos.y = prevPos.y + newVel.y * dt;
  newPos.z = prevPos.z + newVel.z * dt;

  posQueue[0] = newPos;
  velQueue[0] = newVel;
}

void telemetry::updatePredictionWithCamera(){
  Vector imuPos, imuVel, difPos, difVel, complVel;
  float tf = 0.7; //increase for camera to have more importance

  imuPos = posQueue[cameraPositionDelay-1];
  imuVel = velQueue[cameraPositionDelay-1];

  difPos.x = imuPos.x - p0.x;
  difPos.y = imuPos.y - p0.y;
  difPos.z = imuPos.z - p0.z;

  //fuse imu and camera data with complementary filter
  complVel.x = imuVel.x * (1-tf) + p0.vx * tf;
  complVel.y = imuVel.y * (1-tf) + p0.vy * tf;
  complVel.z = imuVel.z * (1-tf) + p0.vz * tf;

  difVel.x = imuVel.x - complVel.x;
  difVel.y = imuVel.y - complVel.y;
  difVel.z = imuVel.z - complVel.z;

  for (int i=0; i<cameraPositionDelay; i++){
    posQueue[i].x -= difPos.x;
    posQueue[i].y -= difPos.y;
    posQueue[i].z -= difPos.z;

    velQueue[i].x -= difPos.x;
    velQueue[i].y -= difPos.y;
    velQueue[i].z -= difPos.z;
  }
}

Vector telemetry::getPredictedPositionIMU(){
  return posQueue[0];
} 

Vector rotateVectorByAngles(Vector vec, Vector angles) {
    double R[3][3], result[3], vector[3];
    double psi = 0, phi = angles.x, theta = angles.y;

    R[0][0] = cos(psi) * cos(theta);
    R[0][1] = cos(psi) * sin(theta) * sin(phi) - sin(psi) * cos(phi);
    R[0][2] = cos(psi) * sin(theta) * cos(phi) + sin(psi) * sin(phi);

    R[1][0] = sin(psi) * cos(theta);
    R[1][1] = sin(psi) * sin(theta) * sin(phi) + cos(psi) * cos(phi);
    R[1][2] = sin(psi) * sin(theta) * cos(phi) - cos(psi) * sin(phi);

    R[2][0] = -sin(theta);
    R[2][1] = cos(theta) * sin(phi);
    R[2][2] = cos(theta) * cos(phi);

    vector[0] = vec.x;  vector[1] = vec.y;  vector[2] = vec.z; 
    for (int i = 0; i < 3; ++i) {
        result[i] = 0;
        for (int j = 0; j < 3; ++j) {
            result[i] += R[i][j] * vector[j];
        }
    }

    Vector out(result[0], result[1], result[2]);

  return out;
}

bool telemetry::processTargetPosition(String s){
  bool b = true, bt = false;
  float tempX = 0, tempY = 0, tempZ = 0;

  bt = getArgFromString(s, "u", tempX);
  b = b && bt;

  bt = getArgFromString(s, "v", tempY);
  b = b && bt;

  bt = getArgFromString(s, "k", tempZ);
  b = b && bt;

  if (abs(tempX - setX) > 0.05 || abs(tempY - setY) > 0.05 || abs(tempZ - setZ) > 0.05){
    if (b && isPositionValid() && tempZ > -0.05 && tempZ < 0.05) { landEnabled = true; } 
    if (b && isPositionValid() && getCameraPosition().z < 0.15 && tempZ > 0.7) { takeOffEnabled = true; }
    if (b && tempX >= minX && tempX <= maxX && tempY >= minY && tempY <= maxY && tempZ >= minZ && tempZ <= maxZ){
      setX = tempX;
      setY = tempY;
      setZ = tempZ;
      this->resetInterpolation(this->getTargetPosition(), false);
    }
  }
  prevSetZ = tempZ;

  return b;
}

Vector telemetry::getCameraPosition(){
  Vector out;
  out.x = positionX;
  out.y = positionY;
  out.z = positionZ;
  return out;
}

Vector telemetry::getTargetPosition(){
  Vector out;
  out.x = setX;
  out.y = setY;
  out.z = setZ;
  return out;
}

void telemetry::resetSetpoint(){
  setX = 0;
  setY = 0;
  setZ = 0;
}

bool telemetry::isPositionValid(){
  if (positionX >= minX && positionX <= maxX && positionY >= minY && positionY <= maxY && positionZ >= minZ && positionZ <= maxZ){
    if (millis() - lastMesTime < mesTimeout){
      return true;
    }
  }
  return false;
}

Vector telemetry::getInterpolatedTargetPosition(){
  float t = ((millis() - interResetTime) / 1000.0f) / interTime;
  return interpolation3D(curTargetPos, targetPos, t);
}

bool telemetry::isInterpolationFinished(){
  return ((millis() - interResetTime) / 1000.0f) > interTime;
}

void telemetry::resetInterpolation(Vector target, bool zero){
  curTargetPos = this->getPredictedPosition();
  targetPos = target;
  interResetTime = millis();
  interTime = vectorDistance(curTargetPos, targetPos) / interSpeed;
  if (zero){
    interTime = 0.0000001;
  }
}

void telemetry::resetInterSpeed(){
  interSpeed = defaultInterSpeed;
}

Vector interpolation3D(Vector v1, Vector v2, float t){
  Vector diff(v2.x - v1.x, v2.y - v1.y, v2.z - v1.z);

  float d = diff.getMagnitude();

  diff.normalize();

  v1.x += t * d * diff.x;
  v1.y += t * d * diff.y;
  v1.z += t * d * diff.z;
  
  return v1;
}

float vectorDistance(Vector v1, Vector v2){
  return sqrt(pow(v1.x-v2.x, 2) + pow(v1.y-v2.y, 2) + pow(v1.z-v2.z, 2));
}

bool getArgFromString(String s, String sf, float &param){
    int pos = -1, searchPos = 0;
    String m = "";
    bool found = false;

    // find the position of the number in the string
    while (s.indexOf(sf, searchPos) != -1){
        pos = s.indexOf(sf, searchPos) + sf.length();
        if (isdigit(s[pos]) || s[pos] == '-'){
            found = true;
            break;
        }
        searchPos = s.indexOf(sf, searchPos) + 1;
    }

    if (!found) { return false; }

    // get the full number from the string
    while ((isdigit(s[pos]) || s[pos] == '.' || s[pos] == '-') && pos<(int)s.length()){
        m += s[pos];
        pos++;
    }
    param = m.toFloat();
    return true;
}
