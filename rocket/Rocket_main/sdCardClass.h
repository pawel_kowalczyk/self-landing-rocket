#ifndef sdCardClass_H
#define sdCardClass_H

#include <Arduino.h>
#include <SD.h>
#include "RingBuf.h"

#define RING_BUF_CAPACITY 10*512
#define LOG_FILE_SIZE 50000000 // preallocated file size 
#define sdFileNumberLimit 50 // maximum number of files on sd card
#define maxDataFields 60
#define writeOutChunk 512 // must be power of 2 (512 is recommended)

class sdCardClass {
  private:
    int sdCardPresent = true;
    String logFileName;
    FsFile dataFile;
    int curLogBuffer = 1;
    RingBuf<FsFile, RING_BUF_CAPACITY> rb;
    RingBuf<FsFile, RING_BUF_CAPACITY> rb2;
    bool finishedLogging = false;
    String dataFields[maxDataFields];
    int dataFieldsNum = 0;
    float logData[maxDataFields];
    bool firstLine = true;
    unsigned long prev_t = 0;
    char floatConv[8];
    bool preAllocateMemory = true;
  
  public:
    const int bufferSyncPeriod = 200; //millisceonds
    sdCardClass();
    String createNewLogFile();
    void logWithThreads(String s);
    void init(bool preAllocateMemory);
    void syncBufferAndCard();
    void finishLogging();
    void logFast();
    void addData(String name, float value, int &index);
    void startLogging();
};

#endif