#pragma once
#include <Arduino.h>
#include <PWMServo.h>

class BLDCMotor{
    private:
        PWMServo motor1, motor2;
        float minPower;
        const int maxOutput = 150;
        int pin1, pin2;
        
    public:
        BLDCMotor();
        void init(int pin1, int pin2);
        void write(float value);
        float mapfloat(float x, float in_min, float in_max, float out_min, float out_max);
};