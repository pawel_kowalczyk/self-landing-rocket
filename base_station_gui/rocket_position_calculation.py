import cv2
import math
import numpy as np
from typing import List, Optional, Tuple

class RocketPositionCalculation:
    """Class for calculating rocket position in space(x,y,z) relative to the camera(0,0,0)."""

    @staticmethod
    def get_rocket_3d_position(
            rgb_frame: np.ndarray,
            depth_frame: np.ndarray,
            camera_parameters: Tuple[float, float, float],
            camera_angle: float,
            mark_positions: bool = True,
            show_coordinates: bool = True
        ) -> Tuple[float, float, float] | None:

        def _change_coord_system(coords) -> np.ndarray:
            """
                From:
                       z
                      /
                     / 
                    0---------x
                    |
                    |
                    |
                    y
                To:
                    Z
                    |  y
                    | /
                    |/
                    0--------x
            """
            return np.array([coords[0], coords[2], -coords[1]])
        
        markers_on_rgb = RocketPositionCalculation.get_rocket_markers(rgb_frame, show_mask=True)
        if markers_on_rgb is None or len(markers_on_rgb) < 2:
            return None
        
        markers_on_depth = [RocketPositionCalculation.map_position(marker, rgb_frame.shape, depth_frame.shape) for marker in markers_on_rgb]

        if len(markers_on_rgb) == 3:
            try:
                position_on_rgb = RocketPositionCalculation.get_center_of_triangle(markers_on_rgb)
            except Exception as ex:
                print(ex)
                return None
        
        elif len(markers_on_rgb) == 2:
            position_on_rgb = np.array(markers_on_rgb).mean(0).astype(np.int16) 
            
        position_on_depth = RocketPositionCalculation.map_position(position_on_rgb, rgb_frame.shape, depth_frame.shape)

        coordinate_z = RocketPositionCalculation.average_distance_from_markers(depth_frame, markers_on_depth, show_masked_result=True)
        coordinates_3D = RocketPositionCalculation.get_absolute_coords(position_on_depth, coordinate_z, *camera_parameters)
        coordinates_3D = RocketPositionCalculation.rotateX(coordinates_3D, camera_angle).flatten()

        coordinates_3D = _change_coord_system(coordinates_3D)
        coordinates_3D = RocketPositionCalculation.translate(coordinates_3D, np.array([0, -2000, 80]))  # in mm

        # Scale from mm to m
        coordinates_3D = [round(coord/1000, 3) for coord in coordinates_3D]

        if mark_positions:
            cv2.circle(rgb_frame, position_on_rgb, 5, (255,255,255), -1)
            cv2.circle(depth_frame, position_on_depth, 5, 0, -1)

        if show_coordinates:
            print("2D (x':{0}, y':{1}) -> 3D (x:{2}, y:{3}, z:{4})".format(*position_on_rgb, *coordinates_3D))

        return coordinates_3D

    @staticmethod
    def get_rocket_markers(frame, show_mask: bool = False, marker_count: int = 3, lower_gray = 150, higher_gray = 255) -> List[Tuple[int, int]] | None:
        """Detect markers on the rocket and return their positions on the frame.
        
        Args:
            frame (frame representation): current frame to detect markers on
        """
        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

        # Threshold the gray image to get only over-exposed pixels
        lower_gray = lower_gray
        higher_gray = higher_gray
        mask = cv2.inRange(gray, lower_gray, higher_gray)
        # mask.resize((640, 480))
        
        if show_mask:
            cv2.imshow("Masked", mask)
        
        # Find contours in the mask
        contours, _ = cv2.findContours(mask, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)

        # Get 3 largesrt contours
        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:marker_count]
        markers = []
        # Iterate through contours and find centroid of each contour
        for contour in contours:
            # Calculate the centroid using the moments of the contour
            M = cv2.moments(contour)
            if M["m00"] != 0:
                cx = int(M["m10"] / M["m00"])
                cy = int(M["m01"] / M["m00"])
                markers.append((cx, cy))
                
        if len(markers):
            return markers
        
        return None

    @staticmethod
    def get_center_position_from_markers(markers: List[Tuple[int, int]]):
        return np.array(markers).mean(0).astype(np.int32)

    @staticmethod
    def get_absolute_coords(point: Tuple[int, int], z: int, cx, cy, f) -> Tuple[float, float, float]:
        """Get absolute position in right hand coordinate system (in mm)."""     

        u, v = point
        x = (u-cx) * z/f
        y = (v-cy) * z/f

        return [x, y, z]

    @staticmethod
    def average_distance_from_markers(depth_frame, markers, shrink_ratio: float = 0.1, show_masked_result: bool = False):
        """Calculates the average distance to the rocket based on markers boundaries (does not take `0`s into account)"""
        
        if len(markers) == 3:
            mask = RocketPositionCalculation._triangle_mask(depth_frame, markers, shrink_ratio, show_masked_result)
        elif len(markers) == 2:
            mask = RocketPositionCalculation._line_mask(depth_frame, markers, shrink_ratio, show_masked_result)
        else:
            raise ValueError(f"Markers count should be 2 or 3, got {len(markers)}")
        
        result = cv2.bitwise_and(depth_frame, mask)
        
        # Extract only non-zero values
        non_zero = result[result!=0]
        
        if show_masked_result:
            for marker in markers:
                cv2.circle(mask, marker, 3, 65535//2, -1)
            cv2.imshow("Masked area", mask)

        # print(non_zero)

        average = non_zero.mean()
        try:
            return np.array([int(average)])
        except:
            return np.array([-1])

    @staticmethod
    def _triangle_mask(depth_frame, markers, shrink_ratio: float = 0.1, show_masked_result: bool = False):
        def _shrink_region(markers, shrink_ration: float = shrink_ratio):
            markers_np = [np.array(marker) for marker in markers]
            center_np = np.array(RocketPositionCalculation.get_center_of_triangle(markers), dtype=np.int16)
            u_vectors = [center_np - marker for marker in markers_np]
            return np.array([(marker + vec*shrink_ration).astype(np.int16).tolist() for marker, vec in zip(markers_np, u_vectors)])
        
        shrinkded_markers = _shrink_region(markers)
        mask = np.zeros_like(depth_frame)
        mask = cv2.drawContours(mask, [shrinkded_markers], -1, 65535, thickness=cv2.FILLED)
        
        return mask
    
    @staticmethod
    def _line_mask(depth_frame: np.ndarray, markers: List[Tuple[int, int]], shrink_ratio: float = 0.1, show_masked_result: bool = False):
        def _shrink_region(markers, shrink_ration: float = shrink_ratio):
            markers_np = [np.array(marker) for marker in markers]
            center_np = np.array(markers_np).mean(0).astype(np.int16)
            u_vectors = [center_np - marker for marker in markers_np]
            return np.array([(marker + vec*shrink_ration).astype(np.int16).tolist() for marker, vec in zip(markers_np, u_vectors)])
        
        shrinked_markers = _shrink_region(markers)

        mask = np.zeros_like(depth_frame)
        cv2.line(mask, *shrinked_markers, 65535, 5)
        
        return mask
            
    @staticmethod
    def map_position(pos: Tuple[int, int], frame_1_shape, frame_2_shape) -> Tuple[int, int]:
        """Mapps position on screen from frame_1 to frame_2."""
        return (int(pos[0]*frame_2_shape[1]/frame_1_shape[1]), int(pos[1]*frame_2_shape[0]/frame_1_shape[0]))
    
    @staticmethod
    def rotateX(vector: np.ndarray, angle: float) -> np.ndarray:
        """
            vec (np.ndarray): point to be rotated
            angle (float): angle in degrees
        """

        cos = math.cos
        sin = math.sin
        angle_r = math.radians(angle)
        rot_mat = np.array(
            [
                [1, 0           ,  0           ],
                [0, cos(angle_r), -sin(angle_r)],
                [0, sin(angle_r),  cos(angle_r)]
            ]
        )
        return rot_mat.dot(vector)
    
    @staticmethod
    def translate(vector: np.ndarray, shift: np.ndarray) -> np.ndarray:
        return vector + shift
    
    @staticmethod
    def get_center_of_triangle(points) -> Tuple[int, int]:
        point1, point2, point3 = points
        # Calculate the midpoints of the line segments defined by the points
        mid1 = ((point1[0] + point2[0]) / 2, (point1[1] + point2[1]) / 2)
        mid2 = ((point2[0] + point3[0]) / 2, (point2[1] + point3[1]) / 2)
        
        # Calculate the slopes of the lines defined by the points
        if point2[1] - point1[1] == 0:
            slope1 = None
        else:
            slope1 = -(point2[0] - point1[0]) / (point2[1] - point1[1])
            
        if point3[1] - point2[1] == 0:
            slope2 = None
        else:
            slope2 = -(point3[0] - point2[0]) / (point3[1] - point2[1])
        
        # Calculate the center coordinates
        if slope1 is None:
            center_x = mid1[0]
            center_y = slope2 * (center_x - mid2[0]) + mid2[1]
        elif slope2 is None:
            center_x = mid2[0]
            center_y = slope1 * (center_x - mid1[0]) + mid1[1]
        else:
            center_x = (slope1 * mid1[0] - slope2 * mid2[0] + mid2[1] - mid1[1]) / (slope1 - slope2)
            center_y = slope1 * (center_x - mid1[0]) + mid1[1]
        
        return (int(center_x), int(center_y))