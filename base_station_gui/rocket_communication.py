from typing import Optional, Tuple
from serialBluetoothConnection import SerialBluetoothConnection as BLTHConnection
import numpy as np

class RocketCommunication:
    def __init__(self, port: int) -> None:
        self.port = port
        self.connection: BLTHConnection
        self.type_error_count = 0

    def setup_connection(self):
        self.connection = BLTHConnection(self.port)
        self.connection.connect()

    def send_current_coordinates(self, time, coordinates: Tuple[float, float, float]):
        msg = "t{}x{}y{}z{}\n".format(time, *coordinates)
        self.safe_send(msg)

    def send_current_and_setpoint(self, time, current_coords: Tuple[float, float, float], setpoint: Tuple[float, float, float]):
        msg = "t{}x{}y{}z{}u{}v{}k{}\n".format(time, *current_coords, *setpoint)
        self.safe_send(msg)

    def rocket_lost(self, time):
        msg = f"t{time}x0y0z-1\n"
        self.safe_send(msg)

    def safe_send(self, msg):
        if self.type_error_count == 5:
            print("Connection lost!")
            self.type_error_count += 1
        elif self.type_error_count < 5:    
            try:
                self.connection.write(msg)
            except TypeError as ex:
                self.type_error_count += 1
                print("Could not send a message")
                print(ex)

    def terminate(self):
        pass

    # def __del__(self):
    #     self.connection.close_connection()
