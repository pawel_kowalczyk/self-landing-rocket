#pragma once

#include <Arduino.h>
#include "mathFunctions.h"
#include <HardwareSerial.h>
// #include <cppQueue.h>

#define cameraPositionDelay 100 //milliseconds

class telemetry{
  public:
    Vector accQueue[cameraPositionDelay], velQueue[cameraPositionDelay], posQueue[cameraPositionDelay];
    struct point{
      float t = 0, x = 0, y = 0, z = 0, vx = 0, vy = 0, vz = 0, ax = 0, ay = 0, az = 0;
    };
    point p0, p1, prev_p0;
    Vector prevPredicted;
    Vector targetPos, curTargetPos;
    Vector accRotated;
    unsigned long prevIntTime = 0;
    float positionX = 0, positionY = 0, positionZ = 0, validPosition = 0, interTime = 0;
    float setX = 0, setY = 0, setZ = 0, prevSetZ;
    float prevX = 0, prevY = 0, prevZ = 0;
    unsigned long lastMesTime = 0, interResetTime = 0;
    float minX = -2, maxX = 2;
    float minY = -2, maxY = 2;
    float minZ = 0, maxZ = 5;
    float mesTimeout = 1000; // milliseconds
    float rawFilterGain = 0.4; // complementary filter
    float predFilterGain = 0.5; // complementary filter
    float accFilterGain = 0.5;
    float predictionTime = 0.12; // seconds
    const float defaultInterSpeed = 0.6; //meters per second
    float interSpeed = defaultInterSpeed; //meters per second
    bool landEnabled = false, takeOffEnabled = false;

    telemetry();
    Vector getCameraPosition();
    bool processString(String s);
    bool update();
    void init();
    bool isPositionValid();
    bool processTargetPosition(String s);
    Vector getTargetPosition();
    Vector getPredictedPosition();
    void resetInterpolation(Vector target, bool zero);
    Vector getInterpolatedTargetPosition();
    bool isInterpolationFinished();
    void resetInterSpeed();
    void resetSetpoint();
    void updatePredictionWithIMU(Vector acc, Vector angle);
    Vector getPredictedPositionIMU();
    void updatePredictionWithCamera();
};