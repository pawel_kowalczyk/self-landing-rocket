#include "bldcmotor.hh"

BLDCMotor::BLDCMotor(){}

void BLDCMotor::init(int pin1, int pin2){
    this->pin1 = pin1;
    this->pin2 = pin2;
    motor1.attach(this->pin1);
    motor2.attach(this->pin2);
    motor1.write(40);
    motor2.write(40);
    Serial.println("Initializing BLDC motors...");
    delay(8000);
}

void BLDCMotor::write(float value){
    if (value < -1 || value > 1){
        Serial.print("Value set outside of the boundry ["); Serial.print(value); Serial.println("].");
        return;
    }
    
    motor1.write(mapfloat(value, -1, 1, 40, maxOutput));
    motor2.write(mapfloat(value, -1, 1, 40, maxOutput));
}

float BLDCMotor::mapfloat(float x, float in_min, float in_max, float out_min, float out_max){
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}