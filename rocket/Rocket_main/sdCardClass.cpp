#include "sdCardClass.h"

sdCardClass::sdCardClass() {

}

void sdCardClass::init(bool preAllocateMemory){
  this->preAllocateMemory = preAllocateMemory;
  Serial.begin(115200);

  if (!SD.sdfs.begin(SdioConfig(FIFO_SDIO))) {
    Serial.println("Card failed, or not present");
    sdCardPresent = false;
    while (1) {}
  }
}

void sdCardClass::startLogging(){
  logFileName = this->createNewLogFile();
  dataFile = SD.sdfs.open(logFileName.c_str(), O_WRITE | O_AT_END | O_CREAT);

  if (preAllocateMemory){
    if (!dataFile.preAllocate(LOG_FILE_SIZE)) {
     Serial.println("preAllocate failed\n");
     dataFile.close();
     return;
    }
  }
  
  rb.begin(&dataFile);
  rb2.begin(&dataFile);
  finishedLogging = false;
  firstLine = true;
  dataFieldsNum = 0;
}

void sdCardClass::addData(String name, float value, int &index){
  if (firstLine){
    dataFields[index] = name;
    dataFieldsNum++;
  }
  logData[index] = value;
  index++;
}

// This logging method doesn't sync the sd card at every write, if power is lost then all data will be lost
// Run the function finishLogging() at the end to save all the data
void sdCardClass::logFast(){ 
  if (finishedLogging) { return; }

  size_t n = rb.bytesUsed();
  if ((n + dataFile.curPosition()) > (LOG_FILE_SIZE - 20)) {
    Serial.println("File full - quitting.");
    finishedLogging = true;
    return;
  }
  prev_t = micros();
  if (n >= writeOutChunk && !dataFile.isBusy()) {
    if (writeOutChunk != rb.writeOut(writeOutChunk)) {
      Serial.println("writeOut failed");
      finishedLogging = true;
      return;
    }
  }
  if (micros()-prev_t>2000){
    Serial.println(micros()-prev_t);
  }


  if (firstLine){
    String data = "";
    for (int i=0; i<dataFieldsNum; i++){
      data += String(dataFields[i]) + " [" + String(i+1) + "]";
      if (i<dataFieldsNum-1){
        data += ",";
      }
    }
    data += "\n";
    rb.print(data.c_str());
    firstLine = false;
  }

  memset(floatConv, 0, sizeof(floatConv));
  for (int i=0; i<dataFieldsNum; i++){
    dtostrf(logData[i], 1, 5, floatConv);
    if (i<dataFieldsNum-1){
      strcat(floatConv, ",");
    }
    rb.print(floatConv);
  }
  rb.print("\n");
}

// Syncs all data to sd card and closes gracefully the file
void sdCardClass::finishLogging(){
  if (!finishedLogging){
    // unsigned long t = micros();
    rb.sync();
    dataFile.truncate();
    dataFile.rewind();
    dataFile.close();
    // Serial.print(micros()-t);
    Serial.println(" Finished logging");
    finishedLogging = true;
  }
}


// This logging method requires a thread running in the background with the function syncBufferAndCard()
// At a power loss the data will still be saved to the sd card
// However the timing can be imprecise because of the threading
// Not recommended to use for frequency analysis
void sdCardClass::logWithThreads(String s) {
  String data = s + "\n";
  if (curLogBuffer == 1){
    rb.print(data.c_str());
  } else{
    rb2.print(data.c_str());
  }
}

// To be used in a thread in the main program 
void sdCardClass::syncBufferAndCard(){
  size_t n1 = rb.bytesUsed();
  size_t n2 = rb2.bytesUsed();
  
  if ((curLogBuffer == 1 && n1>0) || (curLogBuffer == 2 && n2>0)){
    if (curLogBuffer == 1) curLogBuffer = 2;
    else curLogBuffer = 1;
    dataFile = SD.sdfs.open(logFileName.c_str(), O_WRITE | O_AT_END | O_CREAT);
    if (dataFile) {
      if (curLogBuffer == 2){
        rb.sync();
      } else{
        rb2.sync();
      }
      dataFile.close();
    } else {
      Serial.println("error opening file");
    }
  }
}

String sdCardClass::createNewLogFile(){
  for (int i=1; i<=sdFileNumberLimit; i++){
    String fileName = "log" + String(i) + ".csv";
    if (!SD.exists(fileName.c_str())){
      Serial.println(fileName + " was created");
      return fileName;
    }
  }

  Serial.println("SD card full");
  while (1){}
}