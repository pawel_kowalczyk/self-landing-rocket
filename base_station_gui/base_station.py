import pathlib
from basler_camera_set import BaslerCameraSet
from rocket_position_calculation import RocketPositionCalculation as RPC
from rocket_communication import RocketCommunication
from dummy_rocket_communication import DummyRocketCommunication
from dummy_basler_camera_set import DummyBaslerCameraSet
from logger import DataLogger
import numpy as np
import pandas as pd

import cv2
import time
from threading import Thread

class BaseStation:
    def __init__(self, camera_angle: float, checkpoints_path: str = None, is_dummy_camera: bool = False, is_dummy_connection: bool = False) -> None:
        self.camera = DummyBaslerCameraSet(camera_angle) if is_dummy_camera else BaslerCameraSet(camera_angle)
        self.rocket_communication = DummyRocketCommunication(99) if is_dummy_connection else RocketCommunication(3)

        self.none_frame_count = 0
        self.lost_rocket_count = 0

        self.logger = DataLogger()
        self.rocket_data = []
        self.is_recording = False
        self.recording_start_time = 0.0
        
        self.setpoint = None 
        # self.rectangle_setpoints = [[0.0,0.0,1.2], [-0.5,0.0,1.2], [-0.5,0.5,1.2], [0.5,0.5,1.2], [0.5,0.0,1.2], [0.0,0.0,1.2]]
        # self.rectangle_setpoints = [[0.0,0.0,1.2], [-0.5,0.5,1.2], [0.5,0.5,1.2], [0.011,0.011,1.2]]
        self.setpoints = []

        self.running = False

    def setup_opencv_windows(self):

        # cv2.namedWindow("frame")
        # cv2.moveWindow("frame", 10, 10)
        # cv2.setWindowProperty("frame", cv2.WND_PROP_TOPMOST, 1)
        
        # cv2.namedWindow("depth")
        # cv2.moveWindow("depth", 650, 10)
        # cv2.setWindowProperty("depth", cv2.WND_PROP_TOPMOST, 1)

        cv2.namedWindow("Masked")
        cv2.moveWindow("Masked", 1290, 10)
        
        cv2.namedWindow("Concatenated")
        cv2.moveWindow("Concatenated", 10, 10)

    def setup_rocket_communication(self):
        self.rocket_communication.setup_connection()

    def setup_camera(self):
        self.camera.setup_camera_set()
 
    def run(self):
        print("Initializing the camera set.")
        self.setup_camera()
        print("Finished initializing the camera set.\n")

        print("Initializing Bluetooth connection.")
        self.setup_rocket_communication()
        print("Finished initializing Bluetooth connection.\n")

        print("Initializing OpenCV windows.")
        self.setup_opencv_windows()
        print("Finished initializing OpenCV windows.\n")
        
        # Enable loops
        self.running = True

        self.main_loop_thread = Thread(target=self.main_loop)
        self.send_setpoint_thread = Thread(target=self.setpoint_loop)

        print("Starting processing loop.\n")
        self.main_loop_thread.start()

        print("Starting setpoint sending loop.\n")
        self.send_setpoint_thread.start()

    def setpoint_loop(self):
        choice = '0'
        i=0
        while self.running:
            if choice == '0':
                print("Menu:")
                print("1 -> Setpoint")
                print("2 -> Start Recording")
                print("3 -> Stop Recording")
                print("4 -> Rectangle")
            
                choice = input(": ")
            if choice == '1':
                print("Press '9' to go back.")
                print("X: <-1.5,1.5>, Y: <0.0,2.0>, Z: <0.0,1.5>")
                setpoint = input("Setpoint (e.g. -0.7 2.1 1.64): ")
                if setpoint == '9':
                    choice = '0'
                    continue

                if len(setpoint.split(' ')) != 3:
                    print(f"Wrong input format, expected (value value value) got [{setpoint}]")
                    self.setpoint = None
                    continue
                
                try:
                    x,y,z = setpoint = [float(i) for i in setpoint.split(' ')]
                except Exception as ex:
                    print(ex)
                    continue

                if (-1.5 < x > 1.5) or (0 < y > 2) or (0 < z > 1.5):
                    print(f"Value outside of workspace expected [X: <-1.5,1.5>, Y: <0.0,2.0>, Z: <0.0,1.5>] got [X:{x}, Y:{y}, Z:{z}]")
                    # self.setpoint = None
                    continue
                
                self.setpoint = setpoint

            elif choice == '2':
                self.is_recording = True
                self.recording_start_time = time.time()
                self.lost_rocket_count = 0
                choice = '0'

            elif choice == '3':
                if not self.is_recording: 
                    choice = '0'
                    continue

                self.is_recording = False
                Thread(target=self.logger.save, args=(self.rocket_data.copy(), )).start()
                self.rocket_data.clear()
                choice = '0'

            elif choice == '4':
                self.setpoint = self.rectangle_setpoints[i]
                i += 1
                if i >= len(self.rectangle_setpoints):
                    i = 0
                
                choice = '0'

            else:
                choice = '0'

    def main_loop(self):
        time_0 = time.time()
        while self.running:
            rgb, depth = self.camera.get_images()
            if any(frame is None for frame in [rgb, depth]):
                self.none_frame_count += 1
                continue
            
            rocket_coordinates = RPC.get_rocket_3d_position(rgb.copy(), depth.copy(), self.camera.get_coefs(), self.camera.camera_angle, mark_positions=True, show_coordinates=False)
            # rocket_coordinates = None 
            if rocket_coordinates is None:
                self.rocket_communication.rocket_lost(round(time.time()-time_0, 3))
                
                if self.is_recording:
                    self.lost_rocket_count += 1
                    sp = self.setpoint or (-4,-4,-4)
                    self.rocket_data.append([round(time.time()-self.recording_start_time, 3),-5,-5,-5,*sp])
                
                cv2.putText(rgb, "Current Position: UNKNOWN!", (10, 50), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0,255,0))

            else:
                if self.setpoint is None:
                    self.rocket_communication.send_current_coordinates(round(time.time()-time_0, 3), rocket_coordinates)
                    cv2.putText(rgb, "Current Setpoint: Not Set", (10, 90), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0,255,0))
                else:
                    self.rocket_communication.send_current_and_setpoint(round(time.time()-time_0, 3), rocket_coordinates, self.setpoint)
                    cv2.putText(rgb, "Current Setpoint: (X:{:.3f}, Y:{:.3f}, Z:{:.3f})".format(*self.setpoint), (10, 90), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0,255,0))

                if self.is_recording:
                    sp = self.setpoint or (-4,-4,-4)
                    self.rocket_data.append([round(time.time()-self.recording_start_time, 3), *rocket_coordinates, *sp])
                
                cv2.putText(rgb, "Current Position: (X:{:.3f}, Y:{:.3f}, Z:{:.3f})".format(*rocket_coordinates), (10, 50), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0,255,0))

            display_depth = cv2.applyColorMap(cv2.convertScaleAbs(depth, alpha=0.03), cv2.COLORMAP_JET)
            
            cv2.putText(rgb, f"Lost Frames: {self.lost_rocket_count}", (10, 130), fontFace = cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, color=(0,255,0))
            
            # cv2.imshow("depth", display_depth)
            # cv2.imshow("frame", rgb)
            cv2.imshow("Concatenated", np.concatenate((rgb, display_depth), axis=1))

            # sleep_time = 0.045
            # elapsed = time.time()-loop_start_time
            # sleep_time = sleep_time - elapsed
            # if sleep_time < 0:
            #     sleep_time = 0

            # time.sleep(sleep_time)

            # print(f"FPS: {int(1/(time.time()-loop_start_time))}")

            if cv2.pollKey() in [ord('q'), 27]:
                self.running = False
                time.sleep(1)
                break

        print(f"Rocket was lost [{self.lost_rocket_count}] times")
        
        self.camera.terminate()
        self.rocket_communication.terminate()
        # sys.stdin.write('5\n')