#include "sdCardClass.h"
#include "TeensyThreads.h"
#include "RXclass.h"
#include "servo.hh"
#include "bldcmotor.hh"
#include "imuClass.h"
#include "pid.h"
#include "pidAngle.h"
#include "pidPosition.h"
#include "filters.h"
#include "mathFunctions.h"
#include "telemetry.h"
#include "legs.h"

// #define dataLoggingEnabled // uncomment to enable data logging to sd card

sdCardClass dataLogger;
RXclass rx;
Servo finB, finF, finL, finR;
BLDCMotor thruster;
imuClass imu;
compassClass compass;
pid PID;
pidAngle PIDangle;
pidPosition PIDposition;
Filter filter_X1, filter_Y1, filter_Z1, filter_X2, filter_Y2, filter_Z2, filter_X3, filter_Y3, filter_Z3;
Filter filter_ax, filter_ay, filter_az;
FFT FFT_X, FFT_Y, FFT_Z;
telemetry baseTelemetry;
Legs legs;

float channels[8];
float throttle, pitch, roll, yaw, outThrottle;
float startYawHeading = 0;
float prev_filtAccZ = 0, filtAccZ, accVelocity = 0;;
float ff, fb, fl, fr;

unsigned long prevLoopTime = 0, prevGyroTime = 0, prevCompassTime = 0, prevTelemetryTime = 0, prev_t = 0, prevCam = 0, logStartTime = 0;
bool prev_armed = false, newCamUpdate = false, landed = false, prevLandEnabled = false, takeOff = false, changeSpeed = false;
float takeOffThrottle = -1;

Vector noiseFreq, noiseMagn, filtered1, filtered2, filtered3, filteredAcc;
Vector attitudeRate, out, finalAttitudeRate, acceleration, errorAttitudeRate, setpointRate, compassOrient, setpointAngle, errorAngle, outAngle, outPosition;

flightModes flightMode = Angle, prev_flightMode = Angle; // Acro (velocity control), Angle (self-levelling), Manual (only in test stand !!!)
throttleModes prev_throttleMode = ManualControl, throttleMode = ManualControl;
legsModes legsMode = Retracted, prev_legsModeRX = Retracted;

double lowpassCutoff = 170; // Hz
double lowpass_Q = 0.707;
double complGain = 0.4;
double notchFilter_Q = 0.7;
uint16_t maxAngularSpeedXY = 70, maxAngularSpeedZ = 100; //degrees per seconds
uint16_t maxBankingAngle = 15; //degrees
double minNoiseMagn = 1.8;
double hoverThrottle = 0.4;
float accFilterGain = 0.5;
float accLowpassCutoff = 5; //Hz

const uint16_t gyroUpdateRate = 4000; // Hz
const uint16_t loopUpdateRate = 1000; // Hz
const uint16_t compassUpdateRate = 200; // Hz
const uint16_t telemetryUpdateRate = 30; // Hz
const uint16_t gyroUpdatePeriod = (int)1000000.0f/gyroUpdateRate; //microseconds
const uint16_t loopUpdatePeriod = (int)1000000.0f/loopUpdateRate; // microseconds
const uint16_t compassUpdatePeriod = (int)1000000.0f/compassUpdateRate; // microseconds
const uint16_t telemetryUpdatePeriod = (int)1000000.0f/telemetryUpdateRate; //microseconds

void setup() {
  Serial.begin(115200);
  baseTelemetry.init();
  rx.init();
  finB.init(2, 84, 60/52);
  finF.init(4, 95, 60/52);
  finL.init(3, 95, 60/52);
  finR.init(5, 75, 60/52);
  imu.init();
  thruster.init(22,23);
  PID.init(loopUpdateRate);
  PIDangle.init(loopUpdateRate, 70, 100);
  PIDposition.init(telemetryUpdateRate);

  filter_X1.init_biquadFilter(FILTER_LPF, lowpassCutoff, loopUpdateRate, lowpass_Q);
  filter_Y1.init_biquadFilter(FILTER_LPF, lowpassCutoff, loopUpdateRate, lowpass_Q);
  filter_Z1.init_biquadFilter(FILTER_LPF, lowpassCutoff, loopUpdateRate, lowpass_Q);

  filter_X2.init_biquadFilter(FILTER_NOTCH, 0, loopUpdateRate, notchFilter_Q);
  filter_Y2.init_biquadFilter(FILTER_NOTCH, 0, loopUpdateRate, notchFilter_Q);
  filter_Z2.init_biquadFilter(FILTER_NOTCH, 0, loopUpdateRate, notchFilter_Q);
  
  filter_X3.init_complementaryFilter(complGain);
  filter_Y3.init_complementaryFilter(complGain);
  filter_Z3.init_complementaryFilter(complGain);

  filter_ax.init_biquadFilter(FILTER_LPF, accLowpassCutoff, loopUpdateRate, lowpass_Q);
  filter_ay.init_biquadFilter(FILTER_LPF, accLowpassCutoff, loopUpdateRate, lowpass_Q);
  filter_az.init_biquadFilter(FILTER_LPF, accLowpassCutoff, loopUpdateRate, lowpass_Q);

  filter_ax.init_complementaryFilter(0.05);
  filter_ay.init_complementaryFilter(0.05);
  filter_az.init_complementaryFilter(0.05);

  FFT_X.init(); 
  FFT_Y.init();  
  FFT_Z.init(); 

  #ifdef dataLoggingEnabled
    dataLogger.init(true);
  #endif

  while (rx.isArmed()) { Serial.println("Arm switch pressed"); delay(500);}
}

void sdCardSync(){
  while (1){
    dataLogger.syncBufferAndCard();
    threads.delay(dataLogger.bufferSyncPeriod);
  }
}

double removeNoisePeak(double val, FFT &fft, Filter &filter, double &noiseFreq, double &noiseMagn){
  double averageMagn = 9999;
  fft.compute(val, noiseFreq, noiseMagn, averageMagn);
  if (noiseMagn > averageMagn*1.5 && noiseMagn > minNoiseMagn){
    filter.biquadFilterUpdate(FILTER_NOTCH, noiseFreq, loopUpdateRate, notchFilter_Q);
  } else {
    noiseFreq = 0;
  }
  return filter.process_biquadFilter(val);
}

void loop() {
  // IMU update task
  if (micros()-prevGyroTime>gyroUpdatePeriod){
    prevGyroTime = micros();
    imu.update();
    imu.takeSampleForAverage();
  }

  // Telemetry update task
  if (micros()-prevTelemetryTime>telemetryUpdatePeriod){
    prevTelemetryTime = micros();
    baseTelemetry.update();
    newCamUpdate = true;
  }


  // Loop update task (PID, filters)
  if (micros()-prevLoopTime>loopUpdatePeriod){
    prevLoopTime = micros();

    attitudeRate = imu.getAttitudeRate();

    filtered1.y = filter_Y1.process_biquadFilter(attitudeRate.y);
    filtered2.y = removeNoisePeak(filtered1.y, FFT_Y, filter_Y2, noiseFreq.y, noiseMagn.y);
    filtered3.y = filter_Y3.process_complementaryFilter(filtered2.y);

    filtered1.x = filter_X1.process_biquadFilter(attitudeRate.x);
    filtered2.x = removeNoisePeak(filtered1.x, FFT_X, filter_X2, noiseFreq.x, noiseMagn.x);
    filtered3.x = filter_X3.process_complementaryFilter(filtered2.x);

    filtered1.z = filter_Z1.process_biquadFilter(attitudeRate.z);
    filtered2.z = removeNoisePeak(filtered1.z, FFT_Z, filter_Z2, noiseFreq.z, noiseMagn.z);
    filtered3.z = filter_Z3.process_complementaryFilter(filtered2.z);

    finalAttitudeRate = filtered3;

    acceleration = imu.getAcceleration();

    imu.updateMadgwickEstimation(finalAttitudeRate, acceleration, compassOrient);

    Vector t2(0,0,0);
    Vector absAngle = imu.getAngleError(t2);
    filteredAcc.x = filter_ax.process_biquadFilter(acceleration.x);
    filteredAcc.y = filter_ay.process_biquadFilter(acceleration.y);
    filteredAcc.z = filter_az.process_biquadFilter(acceleration.z);
    baseTelemetry.updatePredictionWithIMU(filteredAcc, absAngle);
    printVector(baseTelemetry.accRotated,"accROtated");
    printVector(baseTelemetry.velQueue[0], "velQueue");
    Serial.println(sqrt(pow(filteredAcc.x, 2)+pow(filteredAcc.y, 2)+pow(filteredAcc.z, 2)), 5);
    printVector(baseTelemetry.getPredictedPositionIMU(), "predictedPos");

    roll = rx.getRX(0); pitch = rx.getRX(1); throttle = rx.getRX(2); yaw = rx.getRX(3);
 
    legsRoutine();
    
    flightMode = rx.getFlightMode();
    if (flightMode != Position){
      landed = false;
      takeOff = false;
      baseTelemetry.takeOffEnabled = false;
      baseTelemetry.landEnabled = false;
      takeOffThrottle = -1;
      baseTelemetry.resetInterSpeed();
    }
    if (flightMode != Position && newCamUpdate){
      PIDposition.setSetpoint(baseTelemetry.getCameraPosition());
      PIDposition.updateInput(baseTelemetry.getCameraPosition());
      PIDposition.getPIDoutput();      PIDposition.resetIntegral();
      newCamUpdate = false;
    }
    if ((flightMode == Position && baseTelemetry.isPositionValid() == false) || landed){
      flightMode = Angle;
    }
    Serial.println(flightMode);
    switch (flightMode){
      case Acro: // Acro mode
        setpointRate.y = -pitch * maxAngularSpeedXY;
        setpointRate.x = -roll * maxAngularSpeedXY;
        setpointRate.z = -yaw * maxAngularSpeedZ;

        errorAttitudeRate.y = setpointRate.y - finalAttitudeRate.y;
        errorAttitudeRate.x = setpointRate.x - finalAttitudeRate.x;
        errorAttitudeRate.z = setpointRate.z - finalAttitudeRate.z;

        PID.updatePIDinput(errorAttitudeRate);
        out = PID.getPIDoutput();

        outThrottle = throttle;
        break;
      case Angle:{ // Self-levelling mode (Angle mode)
        setpointAngle.y = -pitch * maxBankingAngle;
        setpointAngle.x = -roll * maxBankingAngle;

        errorAngle = imu.getAngleError(setpointAngle);
        startYawHeading -= yaw/10.0f / 180 * M_PI;
        errorAngle.z = constrainRad(errorAngle.z - startYawHeading);
        PIDangle.updatePIDinput(errorAngle);
        outAngle = PIDangle.getPIDoutput();

        setpointRate.y = outAngle.y;
        setpointRate.x = outAngle.x;
        setpointRate.z = -yaw * maxAngularSpeedZ;

        errorAttitudeRate.y = setpointRate.y - finalAttitudeRate.y;
        errorAttitudeRate.x = setpointRate.x - finalAttitudeRate.x;
        errorAttitudeRate.z = setpointRate.z - finalAttitudeRate.z;

        PID.updatePIDinput(errorAttitudeRate);

        out = PID.getPIDoutput();
        outThrottle = throttle;
      }break;
      case Position:{ // Position control from camera
        if (prev_flightMode != Position && flightMode == Position){
          PIDposition.setSetpoint(baseTelemetry.getCameraPosition());
          PIDposition.updateInput(baseTelemetry.getCameraPosition());
          PIDposition.getPIDoutput();
          PIDposition.resetIntegral();
          baseTelemetry.resetInterpolation(baseTelemetry.getCameraPosition(), true);
        }

        landRoutine();

        if (baseTelemetry.isInterpolationFinished()){
          Vector temp(roll, pitch, throttle);
          PIDposition.integrateSetpoint(temp);
        }


        Serial.print("hoverThrottle");
        Serial.println(hoverThrottle);
        if (newCamUpdate) {
          if (!baseTelemetry.isInterpolationFinished()){
            PIDposition.setSetpoint(baseTelemetry.getInterpolatedTargetPosition());
          }
          PIDposition.updateInput(baseTelemetry.getPredictedPosition());
          outPosition = PIDposition.getPIDoutput();
          setpointAngle.y = -outPosition.y;
          setpointAngle.x = -outPosition.x;
          outThrottle = hoverThrottle + outPosition.z;
          newCamUpdate = false;
        }

        takeOffRoutine();
      
        printVector(baseTelemetry.getTargetPosition(), "target");

        errorAngle = imu.getAngleError(setpointAngle);
        startYawHeading -= yaw/10.0f / 180 * M_PI;
        errorAngle.z = constrainRad(errorAngle.z - startYawHeading);
        PIDangle.updatePIDinput(errorAngle);
        outAngle = PIDangle.getPIDoutput();

        setpointRate.y = outAngle.y;
        setpointRate.x = outAngle.x;
        setpointRate.z = -yaw * maxAngularSpeedZ;

        errorAttitudeRate.y = setpointRate.y - finalAttitudeRate.y;
        errorAttitudeRate.x = setpointRate.x - finalAttitudeRate.x;
        errorAttitudeRate.z = setpointRate.z - finalAttitudeRate.z;

        PID.updatePIDinput(errorAttitudeRate);

        out = PID.getPIDoutput();
        }break;
      case Manual: // Manual operation mode
        out.x = roll;
        out.y = pitch;
        out.z = yaw;
        outThrottle = throttle;
        break;
    }
    prev_flightMode = flightMode;
    
    // check if rocket was recently armed
    if (prev_armed == false && rx.isArmed()){
      Vector t(0,0,0);
      startYawHeading = imu.getAngleError(t).z; // * 180/M_PI;
      PID.resetIntegral();
      while (rx.getFlightMode() == Position) {
        Serial.println("disable position control");
      }
      #ifdef dataLoggingEnabled
        dataLogger.startLogging();
        logStartTime = micros();
      #endif
    }

    if (prev_armed == true && !rx.isArmed()){
      #ifdef dataLoggingEnabled
        dataLogger.finishLogging();
      #endif
    }
    prev_armed = rx.isArmed();

    Serial.print("outThrottle: ");  
    Serial.println(outThrottle);

    if (!rx.isFailsafe() && rx.isArmed()){
      fb = constrain((out.x + out.z)/1, -1, 1);
      ff = constrain((-out.x + out.z)/1, -1, 1);
      fr = constrain((out.y + out.z)/1, -1, 1);
      fl = constrain((-out.y + out.z)/1, -1, 1);
      finB.write((out.x + out.z)/1);
      finF.write((-out.x + out.z)/1);
      finR.write((out.y + out.z)/1);
      finL.write((-out.y + out.z)/1);
      thruster.write(outThrottle);
    } else{
      finB.write(0);
      finF.write(0);
      finR.write(0);
      finL.write(0);
      thruster.write(-1);
    }

    #ifdef dataLoggingEnabled
      if (!rx.isFailsafe() && rx.isArmed()){
        dataLoggerUpdate();
      }
    #endif
    
  }
}

void takeOffRoutine(){
  Serial.print("takeOffEnabled: ");  
  Serial.println(baseTelemetry.takeOffEnabled);
  if (baseTelemetry.getCameraPosition().z < 0.12 && baseTelemetry.getTargetPosition().z > 0.7 && !takeOff && baseTelemetry.takeOffEnabled){
    Serial.println("taking off");
    if (takeOffThrottle < 0.5){
      takeOffThrottle += 4.0f / 10000.0f;
    }
    setpointAngle.y = 0;
    setpointAngle.x = 0;
    outThrottle = takeOffThrottle;
  } else{
    if (!takeOff && baseTelemetry.getTargetPosition().z > 0.7 && baseTelemetry.takeOffEnabled){
      Serial.println("take off done");
      legsMode = Retracted;
      hoverThrottle = takeOffThrottle-0.18;
      outThrottle = hoverThrottle;
      takeOff = true;
      takeOffThrottle = -1;
      PIDposition.resetIntegral();
    }
  }
}

void landRoutine(){
  filtAccZ = acceleration.z * accFilterGain + prev_filtAccZ * (1-accFilterGain);
  prev_filtAccZ = filtAccZ;

  accVelocity += (acceleration.z-1.01) * 0.001 * 9.8;

  if (baseTelemetry.landEnabled){
    if (!prevLandEnabled){ // set land position
      Vector landPosition(baseTelemetry.getCameraPosition().x, baseTelemetry.getCameraPosition().y, 0);
      baseTelemetry.interSpeed = 0.25;
      baseTelemetry.resetInterpolation(landPosition, false);
      changeSpeed = false;
      accVelocity = 0;
    }

    if (landed){
      Serial.print("landed");
    } else{
      if (baseTelemetry.getPredictedPosition().z < 0.9){
        legsMode = ExtendedWithForce; // ExtendedWithForce
      }
      if (baseTelemetry.getPredictedPosition().z < 0.5 && !changeSpeed){
        baseTelemetry.interSpeed = 0.45;
        Vector landPosition(baseTelemetry.getCameraPosition().x, baseTelemetry.getCameraPosition().y, -2);
        baseTelemetry.resetInterpolation(landPosition, false);
        changeSpeed = true;
        accVelocity = 0;
      }
      if (baseTelemetry.getPredictedPosition().z < 0.3 && accVelocity > 0.2){
        Serial.print("landed");
        landed = true;
      }
    }
  }
  prevLandEnabled = baseTelemetry.landEnabled;
}

void legsRoutine(){
  legsModes legsModeRX = rx.getLegsMode();

  if (prev_legsModeRX != legsModeRX){
    legsMode = legsModeRX;
  }
  prev_legsModeRX = legsModeRX;

  switch (legsMode){
    case Extended: 
      legs.setKeepForce(false);
      legs.extend();
      break;
    case ExtendedWithForce:
      legs.setKeepForce(true);
      legs.extend();
      break;
    case Retracted:
      legs.retract();
      break;
  }
  legs.update();
}

void dataLoggerUpdate(){

  Vector pitchComponents, rollComponents, yawComponents;
  PIDposition.getPIDcomponents(pitchComponents, rollComponents, yawComponents);

  int index = 0;
  dataLogger.addData("Time", (micros()-logStartTime)/1000000.0, index);
  dataLogger.addData("f Mode", (flightMode-1) * 5, index);

  dataLogger.addData("cam Z", baseTelemetry.getPredictedPosition().z, index);
  dataLogger.addData("setCam", PIDposition.SetpointZ, index);
  dataLogger.addData("outCam", outPosition.z, index);
  dataLogger.addData("outThrottle", outThrottle, index);
  dataLogger.addData("takeOff", takeOff * 5, index);
  dataLogger.addData("p", yawComponents.x, index);
  dataLogger.addData("i", yawComponents.y, index);
  dataLogger.addData("d", yawComponents.z, index);

  dataLogger.logFast();
}

int addModelData(int index){
  dataLogger.addData("throttle", outThrottle, index);
  dataLogger.addData("finB", fb, index);
  dataLogger.addData("finF", ff, index);
  dataLogger.addData("finR", fr, index);
  dataLogger.addData("finL", fl, index);

  dataLogger.addData("accX", acceleration.x, index);
  dataLogger.addData("accY", acceleration.y, index);
  dataLogger.addData("accZ", acceleration.z, index);

  dataLogger.addData("gyroX", finalAttitudeRate.x, index);
  dataLogger.addData("gyroY", finalAttitudeRate.y, index);
  dataLogger.addData("gyroZ", finalAttitudeRate.z, index);

  Vector t2(0,0,0);
  Vector angle = imu.getAngleError(t2);

  dataLogger.addData("angleX", angle.x, index);
  dataLogger.addData("angleY", angle.y, index);
  dataLogger.addData("angleZ", angle.z, index);

  return index;
}

float constrainRad(float a){
    if (a > M_PI){
    a -= 2 * M_PI;
  } else if (a < -M_PI){
    a += 2 * M_PI;
  }
  return a;
}

void printVector(Vector v, String name){
  Serial.print(name + ": ");

  Serial.print(v.x, 3); Serial.print(" ");
  Serial.print(v.y, 3); Serial.print(" ");
  Serial.print(v.z, 3); Serial.println();
}

void printVectorRad(Vector v, String name){
  Serial.print(name + ": ");

  Serial.print(v.x*180/M_PI); Serial.print(" ");
  Serial.print(v.y*180/M_PI); Serial.print(" ");
  Serial.print(v.z*180/M_PI); Serial.println();
}