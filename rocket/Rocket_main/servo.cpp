#include "servo.hh"
#include <Arduino.h>

Servo::Servo(){}

void Servo::init(int pin, float center, float calib){
    this->pin = pin;
    this->center = center;
    this->calibration = calib;
    servo.attach(this->pin);
    servo.write(this->center);
}

void Servo::write(float value){
    if ( value < -1 || value > 1 ){
        // Serial.print("Value set outside of the boundry ["); Serial.print(value); Serial.println("].");
        return;
    }

    angle = mapfloat(value, -1, 1, center-max_angle, center+max_angle);
    if (angle>center){
      angle = (angle - center) *calibration + center;
    }

    servo.write(angle);
}

float Servo::mapfloat(float x, float in_min, float in_max, float out_min, float out_max){
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}