#pragma once

#include <Arduino.h>

class Legs{

  public:
    int motorPinA = 37, motorPinB = 36;
    uint extendTime = 3000, retractTime = 1600;
    int keepForceValue = 100;
    bool legsExtended = false, keepForce = false; 
    unsigned long prev_t = 0;

    void extend(){ 
      if (legsExtended == false && (millis()-prev_t) > retractTime){
        legsExtended = true;
        prev_t = millis();
      }  
    }

    void retract(){
      if (legsExtended == true && (millis()-prev_t) > extendTime){
        legsExtended = false;
        prev_t = millis();
      }  
    }

    void setKeepForce(bool b){
      keepForce = b;
    }

    void update(){
      if (legsExtended == true && (millis()-prev_t) < extendTime){
        analogWrite(motorPinA, 0);
        analogWrite(motorPinB, 255);
      } else if (legsExtended == true && (millis()-prev_t) > extendTime && keepForce){
        analogWrite(motorPinA, 0);
        analogWrite(motorPinB, keepForceValue);
      } else if (legsExtended == false && (millis()-prev_t) < retractTime){
        analogWrite(motorPinA, 255);
        analogWrite(motorPinB, 0);
      } else{
        analogWrite(motorPinA, 0);
        analogWrite(motorPinB, 0);
      }
    } 

};