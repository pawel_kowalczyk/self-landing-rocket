#include "filters.h"

Filter::Filter(){}

FFT::FFT(){}

void FFT::init(){
  minFFT_frequency_num = (int) round(minFFT_frequency * FFT_sample_length / samplingFrequency);
  for (int i=0; i<FFT_sample_length; i++){
    prevResult[i] = 0;
  }
}

void FFT::compute(double newValue, double &peak1, double &magn1, double &averageMagn){
  // shift back all of the values in the array and add the new value at the end
  for (int i=1; i<FFT_sample_length; i++){
    FFT_queue[i-1] = FFT_queue[i];
  }
  FFT_queue[FFT_sample_length-1] = newValue;

  // if didn't gather all the samples then don't do anything
  if (sampleCount < fftUpdateSampleNumber){
    sampleCount++;
    return;
  }
  sampleCount = 1;

  // copy all the samples to the FFT array
  for (int i=0; i<FFT_sample_length; i++){
    vReal[i] = FFT_queue[i];
    vImag[i] = 0.0;
  }

  // compute the FFT
  fft.windowing(FFTWindow::Hann, FFTDirection::Forward);	// Weigh data
  fft.compute(FFTDirection::Forward);
  fft.complexToMagnitude();

  //  zero the first few frequencies as those represent the movement of IMU and are not noise
  for (int i=0; i<minFFT_frequency_num; i++ ){
    vReal[i] = 0;
  }

  // apply complementary filter to smooth the FFT
  double magnSum = 0;
  for (int i=minFFT_frequency_num; i<FFT_sample_length/2; i++){
    vReal[i] = vReal[i]*FFT_FilterGain + prevResult[i]*(1-FFT_FilterGain) ;
    prevResult[i] = vReal[i];

    magnSum += vReal[i];
  }
  averageMagn = magnSum/(FFT_sample_length/2);

  #ifdef displayFFTdata
  // display FFT once every couple times
  if (count == 4){
      double freq = 0;
      for (int i=0; i<FFT_sample_length/4; i++){
        freq = (i * 1.0 * samplingFrequency) / FFT_sample_length;
        Serial.print(freq);
        Serial.print(",");
        Serial.println(vReal[i], 4);
      }
      Serial.println("a");
      count = 0;
    } else{
      count++;
    }
    #endif
  
  // find the peak frequencies to be filtered
  fft.majorPeak(&peak1, &magn1);

  int peakSample = (int) round(peak1 * FFT_sample_length / samplingFrequency);
  int minPeakSample = peakSample - peakRemoveSamples;
  int maxPeakSample = peakSample + peakRemoveSamples;
  for (int i=minPeakSample; i<=maxPeakSample; i++){
    vReal[i] = 0;
  }

  double peak2, magn2;
  fft.majorPeak(&peak2, &magn2);
}

void FFT::findPeakManual(){
  // filter data backwards then forwards to achieve a zero-phase filter, then find the derivative
  double tf = 1;
  for (int i=1; i<FFT_sample_length/2; i++){
    vReal[i] = tf * vReal[i] + (1-tf) * vReal[i-1];
  }

  for (int i=FFT_sample_length/2-1; i>=1; i--){
    vReal[i-1] = tf * vReal[i-1] + (1-tf) * vReal[i];
    vDeriv[i] = vReal[i]-vReal[i-1];
  }

  // find when derivative goes from positive to negative
  // and find the first 3 largest values
  double maxMagn1 = 0, maxMagn2 = 0, maxMagn3 = 0;
  double maxFreq1 = 0, maxFreq2 = 0, maxFreq3 = 0;
  for (int i=1; i<FFT_sample_length/2; i++){
    double freq = (i * 1.0 * samplingFrequency) / FFT_sample_length;
    if (vDeriv[i]<0 && vDeriv[i-1]>0){
      if (vReal[i]>maxMagn1){
        maxMagn3 = maxMagn2;
        maxMagn2 = maxMagn1;
        maxMagn1 = vReal[i];
        maxFreq3 = maxFreq2;
        maxFreq2 = maxFreq1;
        maxFreq1 = freq;
      } else if (vReal[i]>maxMagn2){
        maxMagn3 = maxMagn2;
        maxMagn2 = vReal[i];
        maxFreq3 = maxFreq2;
        maxFreq2 = freq;
      } else if (vReal[i]>maxMagn3){
        maxMagn3 = vReal[i];
        maxFreq3 = freq;
      }
    }
  }

  double minFreq = 9999;
  if(maxFreq1<maxFreq2){
    minFreq = maxFreq1;
  } else{
    minFreq = maxFreq2;
  }
  if (maxFreq3<minFreq){
    minFreq = maxFreq3;
  }

  Serial.println(minFreq);
}

void Filter::init_biquadFilter(filterType type, float filterFreq, uint32_t samplingFreq, float Q){
  x1 = 0; x2 = 0; y1 = 0; y2 = 0;
  biquadFilterUpdate(type, filterFreq, samplingFreq, Q);
}

void Filter::biquadFilterUpdate(filterType type, float filterFreq, uint32_t samplingFreq, float Q){
    // setup variables
    const double omega = 2.0f * M_PI * filterFreq / samplingFreq;
    const double sn = sin(omega);
    const double cs = cos(omega);
    const double alpha = sn / (2.0f * Q);

    switch (type) {
    case FILTER_LPF:
        // Butterworth biquad section with Q
        // description: http://www.ti.com/lit/an/slaa447/slaa447.pdf
        b1 = 1 - cs;
        b0 = b1 * 0.5f;
        b2 = b0;
        a1 = -2 * cs;
        a2 = 1 - alpha;
        break;
    case FILTER_NOTCH:
        b0 = 1;
        b1 = -2 * cs;
        b2 = 1;
        a1 = b1;
        a2 = 1 - alpha;
        break;
    case FILTER_BPF:
        b0 = alpha;
        b1 = 0;
        b2 = -alpha;
        a1 = -2 * cs;
        a2 = 1 - alpha;
        break;
    }

    const double a0 = 1 + alpha;

    // precompute the coefficients
    b0 /= a0;
    b1 /= a0;
    b2 /= a0;
    a1 /= a0;
    a2 /= a0;
}

double Filter::process_biquadFilter(double input){
    const double result = b0 * input + b1 * x1 + b2 * x2 - a1 * y1 - a2 * y2;

    /* shift x1 to x2, input to x1 */
    x2 = x1;
    x1 = input;

    /* shift y1 to y2, result to y1 */
    y2 = y1;
    y1 = result;

    return result;
}

void Filter::init_complementaryFilter(float complGain){
  this->complGain = complGain;
}

double Filter::process_complementaryFilter(double val){
  complFilterNew = val * complGain + complFilterPrev * (1-complGain);
  complFilterPrev = complFilterNew;
  return complFilterNew;
}