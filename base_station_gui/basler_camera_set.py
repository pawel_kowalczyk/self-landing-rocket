from typing import List, Tuple
from color_and_depth import ColorAndDepth
from delayqueue import DelayQueue, NotReady, Empty, Full
import time
import numpy as np
import cv2

class BaslerCameraSet:
    def __init__(self, camera_angle: float):
        self.color_depth = ColorAndDepth()
        
        self.camera_angle = camera_angle
        
        self.rgb = None
        self.depth = None

        self.running = False

    def setup_camera_set(self):
        self.color_depth.setup_blaze()
        self.color_depth.setup_2Dcamera()
        self.running = True

    def get_images(self) -> Tuple[np.ndarray, np.ndarray]:
        self.color_depth.ia_gev.remote_device.node_map.TriggerSoftware.execute()
        self.color_depth.ia_blaze.remote_device.node_map.TriggerSoftware.execute()
        
        rgb = self.color_depth.get_image_2DCamera()
        _, __, self.depth = self.color_depth.get_image_blaze()
        self.rgb = cv2.resize(rgb[120:880, 150:1130], (640, 480)).reshape((480, 640, 3))
        self.depth.reshape((480, 640))

        return [self.rgb, self.depth]

    def get_coefs(self) -> list:
        return self.color_depth.get_coefs()

    def terminate(self):
        self.running = False
        self.color_depth.terminate()

    def __del__(self):
        self.terminate()