from typing import Tuple

class DummyRocketCommunication:
    def __init__(self, port: int, quiet: bool = True) -> None:
        self.quiet = quiet

    def setup_connection(self):
        pass

    def send_current_coordinates(self, time, coordinates: Tuple[float, float, float]):
        msg = "t{}x{}y{}z{}\n".format(time, *coordinates)
        if not self.quiet:
            print(msg)

    def send_current_and_setpoint(self, time, current_coords: Tuple[float, float, float], setpoint: Tuple[float, float, float]):
        msg = "t{}x{}y{}z{}u{}v{}k{}\n".format(time, *current_coords, *setpoint)
        if not self.quiet:
            print(msg)

    def rocket_lost(self, time):
        msg = f"t{time}x0y0z-1\n"
        if not self.quiet:
            print(msg)
    
    def terminate(self):
        pass

    # def __del__(self):
    #     self.connection.close_connection()
