#include "pid.h"

void extractFloat(String s, float &parameter);
String readSerial();
void printPidCoef(String m, PID x);

pid::pid():
  pidX(&InputX, &OutputX, &SetpointX, 0, 0, 0, DIRECT),
  pidY(&InputY, &OutputY, &SetpointY, 0, 0, 0, DIRECT),
  pidZ(&InputZ, &OutputZ, &SetpointZ, 0, 0, 0, DIRECT) {}

void pid::init(uint32_t loopUpdateRate){
  pidX.SetOutputLimits(-1, 1);
  pidY.SetOutputLimits(-1, 1);
  pidZ.SetOutputLimits(-1, 1);
  pidX.SetSampleTime(1);
  pidY.SetSampleTime(1);
  pidZ.SetSampleTime(1);
  pidX.SetMode(AUTOMATIC);
  pidY.SetMode(AUTOMATIC);
  pidZ.SetMode(AUTOMATIC);
  pidX.SetTunings(X_kp, X_ki, X_kd);
  pidY.SetTunings(Y_kp, Y_ki, Y_kd);
  pidZ.SetTunings(Z_kp, Z_ki, Z_kd);
  pidX.init_DerivativeFilterBiquad(derivativeFilterCutoff, loopUpdateRate);
  pidY.init_DerivativeFilterBiquad(derivativeFilterCutoff, loopUpdateRate);
  pidZ.init_DerivativeFilterBiquad(derivativeFilterCutoff, loopUpdateRate);
}

void pid::updatePIDinput(Vector error){
  InputX = error.x/1000.0;
  InputY = error.y/1000.0;
  InputZ = error.z/1000.0;
}

Vector pid::getPIDoutput(){
  pidX.Compute();
  pidY.Compute();
  pidZ.Compute();
  Vector out(OutputX, OutputY, OutputZ);
  return out;
}

void pid::getPIDcomponents(Vector &pitch, Vector &roll, Vector &yaw){
  pitch.x = pidY.get_proportional();
  pitch.y = pidY.get_integral();
  pitch.z = pidY.get_derivative();

  roll.x = pidX.get_proportional();
  roll.y = pidX.get_integral();
  roll.z = pidX.get_derivative();

  yaw.x = pidZ.get_proportional();
  yaw.y = pidZ.get_integral();
  yaw.z = pidZ.get_derivative();
}

void pid::resetIntegral(){
  pidY.resetIntegral();
  pidX.resetIntegral();
  pidZ.resetIntegral();
}

void pid::tuneUsingSerial(){
  String s = readSerial();
  if (s == ""){ return; }

  if (s[0] == 'p' && s[1] == 'x') { extractFloat(s, X_kp); pidX.SetTunings(X_kp, X_ki, X_kd); printPidCoef("pidX:", pidX);}
  else if (s[0] == 'i' && s[1] == 'x') { extractFloat(s, X_ki); pidX.SetTunings(X_kp, X_ki, X_kd); printPidCoef("pidX:", pidX);}
  else if (s[0] == 'd' && s[1] == 'x') { extractFloat(s, X_kd); pidX.SetTunings(X_kp, X_ki, X_kd); printPidCoef("pidX:", pidX);}
  else if (s[0] == 'p' && s[1] == 'y') { extractFloat(s, Y_kp); pidY.SetTunings(Y_kp, Y_ki, Y_kd); printPidCoef("pidY:", pidY);}
  else if (s[0] == 'i' && s[1] == 'y') { extractFloat(s, Y_ki); pidY.SetTunings(Y_kp, Y_ki, Y_kd); printPidCoef("pidY:", pidY);}
  else if (s[0] == 'd' && s[1] == 'y') { extractFloat(s, Y_kd); pidY.SetTunings(Y_kp, Y_ki, Y_kd); printPidCoef("pidY:", pidY);}
  else if (s[0] == 'p' && s[1] == 'z') { extractFloat(s, Z_kp); pidZ.SetTunings(Z_kp, Z_ki, Z_kd); printPidCoef("pidZ:", pidZ);}
  else if (s[0] == 'i' && s[1] == 'z') { extractFloat(s, Z_ki); pidZ.SetTunings(Z_kp, Z_ki, Z_kd); printPidCoef("pidZ:", pidZ);}
  else if (s[0] == 'd' && s[1] == 'z') { extractFloat(s, Z_kd); pidZ.SetTunings(Z_kp, Z_ki, Z_kd); printPidCoef("pidZ:", pidZ);}
  else if (s[0] == 'd' && s[1] == 'f') { 
    extractFloat(s, derivativeFilterGain); 
    pidX.init_DerivativeFilterCompl(derivativeFilterGain);
    pidY.init_DerivativeFilterCompl(derivativeFilterGain);
    pidZ.init_DerivativeFilterCompl(derivativeFilterGain);
    Serial.print("derivative compl gain = "); Serial.println(derivativeFilterGain);
  } else if (s[0] == 'd' && s[1] == 'b') { 
    extractFloat(s, derivativeFilterCutoff); 
    pidX.update_DerivativeFilterBiquad(derivativeFilterCutoff);
    pidY.update_DerivativeFilterBiquad(derivativeFilterCutoff);
    pidZ.update_DerivativeFilterBiquad(derivativeFilterCutoff);
    Serial.print("derivative biquad cutoff = "); Serial.println(derivativeFilterCutoff);
  }
}

void extractFloat(String s, float &parameter){
  unsigned int i = 2;
  String m = "";

  while ((isDigit(s[i]) || s[i] == '-' || s[i] == '.') && i < s.length()) {
    m += s[i];
    i++;
  }

  if (m != "") {
    parameter = m.toFloat();
  }
}

String readSerial() {
  if (Serial.available()) {
    String s = Serial.readStringUntil('\n');
    Serial.flush();
    return s;
  } else {
    return "";
  }
}

void printPidCoef(String m, PID x){
  Serial.println(m);
  Serial.println(x.GetKp());
  Serial.println(x.GetKi());
  Serial.println(x.GetKd());
}