"""
    author: Anju Rani
    page: https://vbn.aau.dk/da/persons/154045
    
This sample illustrates how to get a depth map from a blaze camera and a color image
from a 2D GigEVision (GEV) color camera using the Python Harvester module.

A Basler blaze camera and a Basler 2D GEV color camera are required for this sample.
"""
import os
import platform
import traceback

# This is used for reshaping the image buffers.
import numpy as np

# This is used for visualization and debayering.
import cv2

# Use of Harvester to access the camera.
# For more information regarding Harvester, visit the github page:
# https://github.com/genicam/harvesters
from harvesters.core import Harvester


BAYER_FORMATS = {"BayerGR8": cv2.COLOR_BayerGR2RGB,
                 "BayerRG8": cv2.COLOR_BayerRG2RGB,
                 "BayerBG8": cv2.COLOR_BayerBG2RGB,
                 "BayerGB8": cv2.COLOR_BayerGB2RGB}


def find_producer(name):
    """ Helper for the GenTL producers from the environment path.
    """
    paths = os.environ['GENICAM_GENTL64_PATH'].split(os.pathsep)

    if platform.system() == "Linux":
        paths.append('/opt/pylon/lib/gentlproducer/gtl/')

    for path in paths:
        path += os.path.sep + name
        if os.path.exists(path):
            return path
    return ""


class ColorAndDepth:
    def __init__(self):
        # Create Harvester instances.
        self.h = Harvester()
        self.cam_matrix = np.array(
            [
            [8.1701870663479656e+02, 0.0, 6.2374232583104515e+02],
            [0.0, 8.1701870663479656e+02, 5.2653483491197778e+02 ],
            [0.0, 0.0, 1.0]
            ],dtype=np.float16)
        self.dist_vector = np.array([-2.5864673721803288e-01, 1.7394961484287297e-01, 0.0, 0.0, 0.0,], dtype=np.float16)
        
        # Location of the Basler blaze GenTL producer.
        if platform.system() == "Windows" or platform.system() == "Linux":
            path_to_blaze_cti = find_producer("ProducerBaslerBlazePylon.cti")
            path_to_gev_cti = find_producer("ProducerGEV.cti")
        else:
            print(f"{platform.system()} is not supported")
            assert False

        # Add producer to Harvester.
        assert os.path.exists(path_to_blaze_cti)
        assert os.path.exists(path_to_gev_cti)

        self.h.add_file(path_to_blaze_cti)
        self.h.add_file(path_to_gev_cti)

        # Update device list.
        self.h.update()

        # Print device list.
        print(self.h.device_info_list)


    def setup_blaze(self):
        # Connect to first blaze camera.
        dev_info = next((d for d in self.h.device_info_list if str(d.model).startswith("blaze")), None)
        if dev_info is not None:
            self.ia_blaze = self.h.create({"model":dev_info.model, "serial_number":dev_info.serial_number})
            print("Connected to blaze-camera: {}".format(
                self.ia_blaze.remote_device.node_map.DeviceSerialNumber.value))
        else:
            print("No blaze camera found.")
            raise RuntimeError

        # Set the component selector.
        # Depth information can be sent either as a 16-bit gray value image or
        # as 3D coordinates (point cloud).
        # For this sample, we want to acquire the depth map.
        # Note: To change the format of an image component, the Component Selector parameter
        # must first be set to the component
        # you want to configure.
        # To use 16-bit integer depth information, choose "Coord3D_C16" instead
        # of "Coord3D_ABC32f".
        self.ia_blaze.remote_device.node_map.ComponentSelector.value = "Range"
        self.ia_blaze.remote_device.node_map.ComponentEnable.value = True
        self.ia_blaze.remote_device.node_map.PixelFormat.value = "Coord3D_C16"

        self.ia_blaze.remote_device.node_map.ComponentSelector.value = "Intensity"
        self.ia_blaze.remote_device.node_map.ComponentEnable.value = True
        self.ia_blaze.remote_device.node_map.PixelFormat.value = "Mono16"

        self.ia_blaze.remote_device.node_map.ComponentSelector.value = "Confidence"
        self.ia_blaze.remote_device.node_map.ComponentEnable.value = True
        self.ia_blaze.remote_device.node_map.PixelFormat.value = "Confidence16"

        # Configure the camera for software triggering.
        self.ia_blaze.remote_device.node_map.TriggerMode.value = "On"
        self.ia_blaze.remote_device.node_map.TriggerSource.value = "Software"

        # Disable GenDC, this mode is currently not supported by the python genicam module
        self.ia_blaze.remote_device.node_map.GenDCStreamingMode.value = "Off"
        
        # Operating mode
        self.ia_blaze.remote_device.node_map.OperatingMode.value = 'LongRange'
        self.ia_blaze.remote_device.node_map.DepthMin.value = 0
        self.ia_blaze.remote_device.node_map.DepthMax.value = 9990

        # Start image acquisition.
        self.ia_blaze.start()

    def setup_2Dcamera(self):
        # Connect to the first available 2D camera, ignore blaze cameras which will
        # be enumerated too.
        dev_info = next((d for d in self.h.device_info_list if 'blaze' not in d.model), None)
        if dev_info is not None:
            self.ia_gev = self.h.create({"serial_number":dev_info.serial_number})
            print("Connected to ace-camera: {}".format(
                dev_info.serial_number))
        else:
            print("No 2D camera found.")
            raise RuntimeError

        # Figure out which 8-bit Bayer pixel format the camera supports.
        # If the camera supports an 8-bit Bayer format, enable the format.
        # Otherwise, exit the program.
        bayer_pattern = next((pf for pf in self.ia_gev.remote_device.node_map.PixelFormat.symbolics
                              if pf in BAYER_FORMATS), None)
        if bayer_pattern is not None:
            self.ia_gev.remote_device.node_map.PixelFormat.value = bayer_pattern
        else:
            print("The camera does not provide Bayer-pattern encoded 8 bit color images")
            raise RuntimeError

        # Configure the camera for software triggering.
        # Each software trigger will start the acquisition of one single frame.
        self.ia_gev.remote_device.node_map.AcquisitionMode.value = "Continuous"
        self.ia_gev.remote_device.node_map.TriggerSelector.value = "FrameStart"
        self.ia_gev.remote_device.node_map.TriggerMode.value = "On"
        self.ia_gev.remote_device.node_map.TriggerSource.value = "Software"

        self.ia_gev.remote_device.node_map.ExposureMode.value = "Timed"
        self.ia_gev.remote_device.node_map.ExposureAuto.value = "Off"
        self.ia_gev.remote_device.node_map.ExposureTimeAbs.value = 10000.0


        # Start image acquisition.
        self.ia_gev.start()

    def close_blaze(self):
        # Stop image acquisition.
        self.ia_blaze.stop()
        self.ia_blaze.remote_device.node_map.TriggerMode.value = "Off"

        # Disconnect from camera.
        self.ia_blaze.destroy()

    def close_2DCamera(self):
        # Stop image acquisition.
        self.ia_gev.stop()
        self.ia_gev.remote_device.node_map.TriggerMode.value = "Off"

        # Disconnect from camera.
        self.ia_gev.destroy()

    def close_harvesters(self):
        # Remove the CTI file and reset Harvester.
        self.h.reset()

    def get_image_blaze(self):
        with self.ia_blaze.fetch() as buffer:
            # Warning: The buffer is only valid in the with statement and will be destroyed
            # when you leave the scope.
            # If you want to use the buffers outside of the with scope, you have to use np.copy()
            # to make a deep copy of the image.

            # Create an alias of the image components:
            pointcloud = buffer.payload.components[0]

            intensity = buffer.payload.components[1]

            confidence = buffer.payload.components[2]

            # Reshape the depth map into a 2D/3D array:
            # "num_components_per_pixel" depends on the pixel format selected:
            # "Coord3D_ABC32f" = 3
            # "Coord3D_C16" = 1
            _3d = pointcloud.data.reshape(pointcloud.height, pointcloud.width,
                                          int(pointcloud.num_components_per_pixel))
            depth = _3d.copy()
            depth = (depth * self.get_gray2mm_scale()).astype(np.uint16)

            # Reshape the intensity image into a 2D array:
            _2d_intensity = intensity.data.reshape(intensity.height, intensity.width)
            intensity = _2d_intensity.copy()

            # Reshape the confidence map into a 2D array:
            _2d_confidence = confidence.data.reshape(confidence.height, confidence.width)
            confidence = _2d_confidence.copy()

        return [intensity, confidence, depth]

    def get_image_2DCamera(self):
        with self.ia_gev.fetch() as buffer:
            # Warning: The buffer is only valid in the with statement and will be destroyed
            # when you leave the scope.
            # If you want to use the buffers outside of the with scope, you have to use np.copy()
            # to make a deep copy of the image.

            # Create an alias of the image component:
            image = buffer.payload.components[0]

            # Reshape the image into a 2D array:
            _2d = image.data.reshape(image.height, image.width)
            _2d = self.undistort(_2d)
            
            # Debayer the image to an RGB image.
            _2d_rgb = cv2.cvtColor(_2d, BAYER_FORMATS[image.data_format])

        return _2d_rgb.copy()

    def undistort(self, rgb_image: np.ndarray):
        h,w = rgb_image.shape[:2]
        new_cam_matrix, roi = cv2.getOptimalNewCameraMatrix(self.cam_matrix, self.dist_vector, (w,h), 1, (w,h))
        undistorted = cv2.undistort(rgb_image, self.cam_matrix, self.dist_vector, None, new_cam_matrix)
        return undistorted
    
    def get_coefs(self):
        """Get cx, cy, f, gray2cm."""
        cx = self.ia_blaze.remote_device.node_map.Scan3dPrincipalPointU.value
        cy = self.ia_blaze.remote_device.node_map.Scan3dPrincipalPointV.value
        f = self.ia_blaze.remote_device.node_map.Scan3dFocalLength.value

        return [cx, cy, f]
    
    def get_gray2cm_scale(self) -> float:
        return self.get_gray2mm_scale()/10
        
    def get_gray2mm_scale(self) -> float:
        return self.ia_blaze.remote_device.node_map.Scan3dCoordinateScale.value #0.152588
        
    def execute_trigger_software(self):
        self.ia_gev.remote_device.node_map.TriggerSoftware.execute()
        self.ia_blaze.remote_device.node_map.TriggerSoftware.execute()
    
    def terminate(self):
        self.close_blaze()
        self.close_2DCamera()
        self.close_harvesters()

    def run(self):
        # Set up the cameras.
        self.setup_blaze()
        self.setup_2Dcamera()

        print('To exit, press ESC in one of the image windows')
        # Grab the images.
        while True:
            # To optimize bandwidth usage, the color camera is triggered first to
            # allow it to already transfer image data while the blaze camera is still internally
            # processing the acquired raw data.
            self.ia_gev.remote_device.node_map.TriggerSoftware.execute()
            self.ia_blaze.remote_device.node_map.TriggerSoftware.execute()

            intensity, confidence, depth = self.get_image_blaze()
            rgb = self.get_image_2DCamera()
            
            cv2.imshow("Depth", depth)
            cv2.imshow("RGB", rgb)
            # Break the endless loop by pressing ESC.
            k = cv2.pollKey()
            if k == 27:
                break

        # Close the camera and release the producers.
        self.close_blaze()
        self.close_2DCamera()
        self.close_harvesters()


if __name__ == "__main__":
    """ Run the sample.
    """
    Sample = ColorAndDepth()
    try:
        Sample.run()
    except Exception:
        traceback.print_exc()
        Sample.close_harvesters()