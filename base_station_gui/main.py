import tkinter as tk
from main_menu import MainMenuApp

def main():
    root = tk.Tk()
    app = MainMenuApp(root, is_dummy_camera=False, is_dummy_connection=False)
    root.mainloop()
    app.terminate_base_station()

if __name__ == "__main__":
    main()