#pragma once

#include <MPU6050_light.h>
#include <QMC5883LCompass.h>
#include "mathFunctions.h"
#include <Wire.h>

class imuClass{
    private:
      MPU6050 mpu;
      void MadgwickQuaternionUpdate(Quaternion &q, float ax, float ay, float az, float gyrox, float gyroy, float gyroz);
      void MadgwickQuaternionUpdateMagn(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz);
      double deltat;
      float zeta = 0.06;
      float beta1 = 1.0, beta2 = 0.15;
      float beta;
      const uint16_t gyroUpdateRate = 4000; // Hz
      const uint16_t gyroUpdatePeriod = (int)1000000.0f/gyroUpdateRate; //microseconds
      unsigned long prevGyro = 0, Now = 0, lastUpdate = 0, startTime = 0;
      double x0, x1, x2, x3, y0, y1, y2, y3, z0, z1, z2, z3; // last 4 samples of gyro data
      bool estimationStarted = false;

    public:
      Quaternion q, target_q, error_q, startQuaternion;
      imuClass();
      void init();
      void update();
      void resetOrientation();
      Vector getAttitude();
      Vector getAngleError(Vector setpointAngle);
      Vector getAttitudeRate();
      Vector getAcceleration();
      void updateMadgwickEstimation(Vector gyro, Vector acc, Vector magn);
      void configureDLPF();
      void takeSampleForAverage();
};

class compassClass{
  private:
    QMC5883LCompass compass;

  public:
    compassClass(){};
    void init(){ 
      compass.init();
      Wire.setClock(1000000);
      compass.setCalibrationOffsets(-1204.00, -147.00, -895.00);
      compass.setCalibrationScales(1.03, 0.98, 0.98);
      compass.setSmoothing(3, true);
    }
    void update(){ 
      compass.read(); 
    }
    Vector getCompass(){
      Vector out(compass.getY(), compass.getX(), -compass.getZ()); // change orientation of compass here z,x,y
      return out;
    }
};

