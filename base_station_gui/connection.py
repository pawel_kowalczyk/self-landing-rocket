from abc import ABC, abstractmethod

class Connection(ABC):
    """Connection Interface"""
    @abstractmethod
    def connect(self) -> None:
        pass

    @abstractmethod
    def write(self, message:str) -> None:
        pass

    @abstractmethod
    def read(self, length:int) -> str:
        pass

    @abstractmethod
    def close_connection(self) -> None:
        pass