import tkinter as tk
from tkinter import ttk
import pathlib
import pandas as pd
import numpy as np
from rocket_path_creator import RocketPathCreator
from base_station_window import BaseStationApp

class MainMenuApp:
    def __init__(self, root, is_dummy_camera=True, is_dummy_connection=True):
        self.is_dummy_camera = is_dummy_camera
        self.is_dummy_connection = is_dummy_connection

        self.root = root
        self.root.title("Main Menu")
        self.root.geometry("480x640")

        # Adjust font size and padding
        self.default_font = ("Arial", 12)
        self.large_font = ("Arial", 16)
        self.default_padding = 20
        self.large_padding = 30

        # Style configuration
        self.style = ttk.Style()
        self.style.configure('Large.TButton', font=self.large_font)
        self.style.configure('Default.TButton', font=self.default_font)

        # Button to start
        self.start_button = ttk.Button(self.root, text="Start", style='Large.TButton', command=self.start_rocket_control)
        self.start_button.pack(pady=self.large_padding)

        # Button to create new path
        self.create_path_button = ttk.Button(self.root, text="Create new path", style='Large.TButton', command=self.start_path_creator)
        self.create_path_button.pack(pady=self.large_padding)

        # Button to load path
        self.load_path_button = ttk.Button(self.root, text="Load paths", command=self.show_path_options, style='Large.TButton')
        self.load_path_button.pack(pady=self.large_padding)

        # Roll-down to display available paths
        self.path_combobox = ttk.Combobox(self.root, state="readonly", font=self.default_font)
        self.path_combobox.pack(pady=self.default_padding)
        self.path_combobox.bind("<<ComboboxSelected>>", self.show_path_description)

        # Description label
        self.description_label = ttk.Label(self.root, text="", font=self.default_font)
        self.description_label.pack(pady=self.default_padding)
        self.description_label.pack_forget()  # Initially hidden


    def start_rocket_control(self):
        self.root.iconify()
        self.base_station_window = tk.Toplevel(self.root)
        self.base_station_window.geometry("+650+515")
        self.base_station = BaseStationApp(self.base_station_window, 23.4, self.is_dummy_camera, self.is_dummy_connection)
        self.base_station_window.protocol("WM_DELETE_WINDOW", self.base_station.terminate)
        self.base_station.run()

    def start_path_creator(self):
        path_creator_window = tk.Toplevel(self.root)
        RocketPathCreator(path_creator_window)

    def load_paths(self):
        self.paths_folder = pathlib.Path(__file__).parent / "rocket_paths"
        paths = [path.stem for path in self.paths_folder.iterdir() if path.suffix == ".csv"]
        return paths    
    
    def show_path_options(self):
        # Assume available paths are fetched from somewhere and stored in a list
        available_paths = self.load_paths()
        self.path_combobox["values"] = available_paths
        self.path_combobox.current(0)  # Select the first option by default

    def show_path_description(self, event):
        selected_path = self.path_combobox.get()
        # Assume description for the selected path is fetched from somewhere
        points = pd.read_csv(pathlib.Path(__file__).parent / "rocket_paths" / f"{selected_path}.csv", sep=',', header=None).values
        # description = np.array_str(points, precision=3, suppress_small=False)
        description = np.array2string(points, formatter={'float_kind':'{0:.3f} '.format})
        self.description_label.config(text=description)
        self.description_label.pack()  # Show the description

    def terminate_base_station(self):
        if hasattr(self, 'base_station'):
            self.base_station.terminate()