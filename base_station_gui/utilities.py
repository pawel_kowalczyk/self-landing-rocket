import numpy as np

class RGDBFrame:

    @staticmethod
    def create(rgb: np.ndarray, depth: np.ndarray) -> np.ndarray:
        # print(1, rgb.shape, depth.shape)
        depth = depth.copy().flatten()
        # print(2, rgb.shape, depth.shape)
        depth = depth.reshape((480, 640))
        # print(3, rgb.shape, depth.shape)
        depth = depth[:, :, np.newaxis]
        # print(4, rgb.shape, depth.shape)
        return np.concatenate((rgb, depth), axis=2)
    
# if __name__ == "__main__":
#     data = np.fromfile("X:\\GitLab\\AIE_Projects\\P4\\self-landing-rocket\\base_station_gui\\results\\result_24-05-2024_19-47-18\\frames\\1_829.bin", dtype=np.uint16)

#     print(data.reshape((480, 640, 4)))