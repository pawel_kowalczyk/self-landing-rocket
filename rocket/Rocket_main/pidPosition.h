#pragma once

#include <Arduino.h>
#include "pidClass.h"
#include "mathFunctions.h"

class pidPosition{
    private:
      PID pidX;  
      PID pidY; 
      PID pidZ; 
      float maxBankingAngle = 10;
      float maxThrottleDiff = 0.4;

      float derivativeFilterGain = 0.3;
      float derivativeFilterCutoff = 1; //Hz
      float derivativeFilterCutoffThr = 0.2; //Hz
      float X_kp = 7.5, X_ki = 60, X_kd = 0.3; // pid coefficients roll
      float Y_kp = 7.5, Y_ki = 60, Y_kd = 0.3; // pid coefficients pitch
      float Z_kp = 0.23, Z_ki = 3, Z_kd = 0.008; // pid coefficients yaw

    public:
      double SetpointX = 0, InputX, OutputX;
      double SetpointY = 0, InputY, OutputY;
      double SetpointZ = 0, InputZ, OutputZ;
      pidPosition();
      void init(uint32_t loopUpdateRate);
      Vector getPIDoutput();
      void tuneUsingSerial();
      void getPIDcomponents(Vector &pitch, Vector &roll, Vector &yaw);
      void resetIntegral();
      void integrateSetpoint(Vector diff);
      void setSetpoint(Vector set);
      void updateInput(Vector in);
};