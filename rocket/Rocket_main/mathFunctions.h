#pragma once

class Quaternion {
    public:
        double w;
        double x;
        double y;
        double z;
        
        Quaternion() {
            w = 1.0f;
            x = 0.0f;
            y = 0.0f;
            z = 0.0f;
        }
        
        Quaternion(double nw, double nx, double ny, double nz) {
            w = nw;
            x = nx;
            y = ny;
            z = nz;
        }

        Quaternion getProduct(Quaternion q) {
            // Quaternion multiplication is defined by:
            //     (Q1 * Q2).w = (w1w2 - x1x2 - y1y2 - z1z2)
            //     (Q1 * Q2).x = (w1x2 + x1w2 + y1z2 - z1y2)
            //     (Q1 * Q2).y = (w1y2 - x1z2 + y1w2 + z1x2)
            //     (Q1 * Q2).z = (w1z2 + x1y2 - y1x2 + z1w2
            return Quaternion(
                w*q.w - x*q.x - y*q.y - z*q.z,  // new w
                w*q.x + x*q.w + y*q.z - z*q.y,  // new x
                w*q.y - x*q.z + y*q.w + z*q.x,  // new y
                w*q.z + x*q.y - y*q.x + z*q.w); // new z
        }

        Quaternion getConjugate() {
            return Quaternion(w, -x, -y, -z);
        }
        
        double getMagnitude() {
            return sqrt(w*w + x*x + y*y + z*z);
        }
        
        void normalize() {
            const double im = 1.0f / getMagnitude();
            w *= im;
            x *= im;
            y *= im;
            z *= im;
        }
        
        Quaternion getNormalized() {
            Quaternion r(w, x, y, z);
            r.normalize();
            return r;
        }
};

class Vector {
    public:
        double x;
        double y;
        double z;

        Vector() {
            x = 0;
            y = 0;
            z = 0;
        }
        
        Vector(double nx, double ny, double nz) {
            x = nx;
            y = ny;
            z = nz;
        }

        double getMagnitude() {
            return sqrt(x*x + y*y + z*z);
        }

        void normalize() {
            const double m = 1.0f / getMagnitude();
            x *= m;
            y *= m;
            z *= m;
        }
        
        Vector getNormalized() {
            Vector r(x, y, z);
            r.normalize();
            return r;
        }
        
        void rotate(Quaternion *q) {
            Quaternion p(0, x, y, z);

            // quaternion multiplication: q * p, stored back in p
            p = q -> getProduct(p);

            // quaternion multiplication: p * conj(q), stored back in p
            p = p.getProduct(q -> getConjugate());

            // p quaternion is now [0, x', y', z']
            x = p.x;
            y = p.y;
            z = p.z;
        }

        Vector getRotated(Quaternion *q) {
            Vector r(x, y, z);
            r.rotate(q);
            return r;
        }
};