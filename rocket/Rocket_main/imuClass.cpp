#include "imuClass.h"
#include <Arduino.h>

Quaternion qfa(float roll, float pitch, float yaw);
Vector afq(Quaternion* q);
Vector afv(Vector v);
void printQuaternion(Quaternion q, bool end);

#define MPU6050_ADDR 0x68
#define REG_CONFIG 0x1A
#define WIRE Wire1

imuClass::imuClass() : mpu(WIRE) {}

void imuClass::init(){
  WIRE.begin();
  WIRE.setClock(1000000);

  byte status = mpu.begin(3,3); // 3, 3
  Serial.print(F("MPU6050 status : ")); Serial.println(status);
  if (status > 0){
    Serial.println("IMU not initialized");
    while (1) { }
  }

  configureDLPF();
  mpu.setAccOffsets(0.06, 0.005, -0.055); //decrease y to move to right
  mpu.setGyroOffsets(-3.43, 0, -0.4);
  q.w = 1.0; q.x = 0; q.y = 0; q.z = 0;
  mpu.fetchData();
  beta = beta1;
}

void imuClass::updateMadgwickEstimation(Vector gyro, Vector acc, Vector magn){
  if (micros()-lastUpdate>1000000) { lastUpdate = micros(); }
  
  // beta is beta1 for the first 3 seconds then beta becomes beta2
  if (!estimationStarted){
    startTime = millis();
    estimationStarted = true;
  } else if (millis()-startTime > 3000){
    beta = beta2;
  }
  
  for(uint8_t i = 0; i < 15; i++) { // iterate a fixed number of times per data read cycle
    Now = micros();
    deltat = ((Now - lastUpdate)/1000000.0f); // set integration time by time elapsed since last filter update
    lastUpdate = Now;
    MadgwickQuaternionUpdate(q, acc.x, acc.y, acc.z, gyro.x*M_PI/180.0f, gyro.y*M_PI/180.0f, gyro.z*M_PI/180.0f);
  }
}

Vector imuClass::getAttitude(){
  return afq(&q);
}

Vector imuClass::getAngleError(Vector setpointAngle){ 
  Quaternion target_q = qfa(setpointAngle.x*M_PI/180.0f, setpointAngle.y*M_PI/180.0f, setpointAngle.z*M_PI/180.0f);

  Quaternion qq = target_q.getProduct(q.getConjugate());
  return afq(&qq);
}

void imuClass::resetOrientation(){
  startQuaternion = q;
}

Vector imuClass::getAttitudeRate(){
  double x = (x0 + x1 + x2 + x3) / 4;
  double y = (y0 + y1 + y2 + y3) / 4;
  double z = (z0 + z1 + z2 + z3) / 4;
  Vector out(x, y, z); //order of the actual vector is x, y, z, change here if IMU orientation is not default 
  return out;
}

void imuClass::update(){
  mpu.fetchData();
}

void imuClass::takeSampleForAverage(){
  x3 = x2;
  x2 = x1;
  x1 = x0;
  x0 = mpu.getGyroX();
  y3 = y2;
  y2 = y1;
  y1 = y0;
  y0 = mpu.getGyroY();
  z3 = z2;
  z2 = z1;
  z1 = z0;
  z0 = mpu.getGyroZ();
}

Vector imuClass::getAcceleration(){
  Vector out(mpu.getAccX(), mpu.getAccY(), mpu.getAccZ()); // (y,z,x)
  return out;
}

void imuClass::configureDLPF() {
  // Read current value of CONFIG register
  WIRE.beginTransmission(MPU6050_ADDR);
  WIRE.write(REG_CONFIG);
  WIRE.endTransmission(false);
  WIRE.requestFrom(MPU6050_ADDR, 1);
  byte currentConfig = WIRE.read();

  // Clear bits 2-0 (DLPF setting)
  byte newConfig = currentConfig & 0b11111000;

  // Write new value to CONFIG register
  WIRE.beginTransmission(MPU6050_ADDR);
  WIRE.write(REG_CONFIG);
  WIRE.write(newConfig);
  WIRE.endTransmission(true);

  // Verify setting
  WIRE.beginTransmission(MPU6050_ADDR);
  WIRE.write(REG_CONFIG);
  WIRE.endTransmission(false);
  WIRE.requestFrom(MPU6050_ADDR, 1);
  byte readBack = WIRE.read();
  if ((readBack & 0b00000111) != 0){
    Serial.println("Error setting DMP");
    while (1) {}
  }
}

void imuClass::MadgwickQuaternionUpdate(Quaternion &q, float ax, float ay, float az, float gyrox, float gyroy, float gyroz){
    float q1 = q.w, q2 = q.x, q3 = q.y, q4 = q.z;         // short name local variable for readability
    float norm;                                               // vector norm
    float f1, f2, f3;                                         // objetive funcyion elements
    float J_11or24, J_12or23, J_13or22, J_14or21, J_32, J_33; // objective function Jacobian elements
    float qDot1, qDot2, qDot3, qDot4;
    float hatDot1, hatDot2, hatDot3, hatDot4;
    float gerrx, gerry, gerrz, gbiasx = 0, gbiasy = 0, gbiasz = 0;        // gyro bias error

    // Auxiliary variables to avoid repeated arithmetic
    float _halfq1 = 0.5f * q1;
    float _halfq2 = 0.5f * q2;
    float _halfq3 = 0.5f * q3;
    float _halfq4 = 0.5f * q4;
    float _2q1 = 2.0f * q1;
    float _2q2 = 2.0f * q2;
    float _2q3 = 2.0f * q3;
    float _2q4 = 2.0f * q4;

    // Normalise accelerometer measurement
    norm = sqrt(ax * ax + ay * ay + az * az);
    if (norm == 0.0f) return; // handle NaN
    norm = 1.0f/norm;
    ax *= norm;
    ay *= norm;
    az *= norm;
    
    // Compute the objective function and Jacobian
    f1 = _2q2 * q4 - _2q1 * q3 - ax;
    f2 = _2q1 * q2 + _2q3 * q4 - ay;
    f3 = 1.0f - _2q2 * q2 - _2q3 * q3 - az;
    J_11or24 = _2q3;
    J_12or23 = _2q4;
    J_13or22 = _2q1;
    J_14or21 = _2q2;
    J_32 = 2.0f * J_14or21;
    J_33 = 2.0f * J_11or24;
  
    // Compute the gradient (matrix multiplication)
    hatDot1 = J_14or21 * f2 - J_11or24 * f1;
    hatDot2 = J_12or23 * f1 + J_13or22 * f2 - J_32 * f3;
    hatDot3 = J_12or23 * f2 - J_33 *f3 - J_13or22 * f1;
    hatDot4 = J_14or21 * f1 + J_11or24 * f2;
    
    // Normalize the gradient
    norm = sqrt(hatDot1 * hatDot1 + hatDot2 * hatDot2 + hatDot3 * hatDot3 + hatDot4 * hatDot4);
    hatDot1 /= norm;
    hatDot2 /= norm;
    hatDot3 /= norm;
    hatDot4 /= norm;
    
    // Compute estimated gyroscope biases
    gerrx = _2q1 * hatDot2 - _2q2 * hatDot1 - _2q3 * hatDot4 + _2q4 * hatDot3;
    gerry = _2q1 * hatDot3 + _2q2 * hatDot4 - _2q3 * hatDot1 - _2q4 * hatDot2;
    gerrz = _2q1 * hatDot4 - _2q2 * hatDot3 + _2q3 * hatDot2 - _2q4 * hatDot1;
    
    // Compute and remove gyroscope biases
    gbiasx += gerrx * deltat * zeta;
    gbiasy += gerry * deltat * zeta;
    gbiasz += gerrz * deltat * zeta;
    gyrox -= gbiasx;
    gyroy -= gbiasy;
    gyroz -= gbiasz;
    
    // Compute the quaternion derivative
    qDot1 = -_halfq2 * gyrox - _halfq3 * gyroy - _halfq4 * gyroz;
    qDot2 =  _halfq1 * gyrox + _halfq3 * gyroz - _halfq4 * gyroy;
    qDot3 =  _halfq1 * gyroy - _halfq2 * gyroz + _halfq4 * gyrox;
    qDot4 =  _halfq1 * gyroz + _halfq2 * gyroy - _halfq3 * gyrox;

    // Compute then integrate estimated quaternion derivative
    q1 += (qDot1 -(beta * hatDot1)) * deltat;
    q2 += (qDot2 -(beta * hatDot2)) * deltat;
    q3 += (qDot3 -(beta * hatDot3)) * deltat;
    q4 += (qDot4 -(beta * hatDot4)) * deltat;

    // Normalize the quaternion
    norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalise quaternion
    norm = 1.0f/norm;
    q.w = q1 * norm;
    q.x = q2 * norm;
    q.y = q3 * norm;
    q.z = q4 * norm;
}

void imuClass::MadgwickQuaternionUpdateMagn(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz){
    float q1 = q.w, q2 = q.x, q3 = q.y, q4 = q.z;   // short name local variable for readability
    float norm;
    float hx, hy, _2bx, _2bz;
    float s1, s2, s3, s4;
    float qDot1, qDot2, qDot3, qDot4;

    // Auxiliary variables to avoid repeated arithmetic
    float _2q1mx;
    float _2q1my;
    float _2q1mz;
    float _2q2mx;
    float _4bx;
    float _4bz;
    float _2q1 = 2.0f * q1;
    float _2q2 = 2.0f * q2;
    float _2q3 = 2.0f * q3;
    float _2q4 = 2.0f * q4;
    float _2q1q3 = 2.0f * q1 * q3;
    float _2q3q4 = 2.0f * q3 * q4;
    float q1q1 = q1 * q1;
    float q1q2 = q1 * q2;
    float q1q3 = q1 * q3;
    float q1q4 = q1 * q4;
    float q2q2 = q2 * q2;
    float q2q3 = q2 * q3;
    float q2q4 = q2 * q4;
    float q3q3 = q3 * q3;
    float q3q4 = q3 * q4;
    float q4q4 = q4 * q4;

    // Normalise accelerometer measurement
    norm = sqrtf(ax * ax + ay * ay + az * az);
    if (norm == 0.0f) return; // handle NaN
    norm = 1.0f/norm;
    ax *= norm;
    ay *= norm;
    az *= norm;

    // Normalise magnetometer measurement
    norm = sqrtf(mx * mx + my * my + mz * mz);
    if (norm == 0.0f) return; // handle NaN
    norm = 1.0f/norm;
    mx *= norm;
    my *= norm;
    mz *= norm;

    // Reference direction of Earth's magnetic field
    _2q1mx = 2.0f * q1 * mx;
    _2q1my = 2.0f * q1 * my;
    _2q1mz = 2.0f * q1 * mz;
    _2q2mx = 2.0f * q2 * mx;
    hx = mx * q1q1 - _2q1my * q4 + _2q1mz * q3 + mx * q2q2 + _2q2 * my * q3 + _2q2 * mz * q4 - mx * q3q3 - mx * q4q4;
    hy = _2q1mx * q4 + my * q1q1 - _2q1mz * q2 + _2q2mx * q3 - my * q2q2 + my * q3q3 + _2q3 * mz * q4 - my * q4q4;
    _2bx = sqrtf(hx * hx + hy * hy);
    _2bz = -_2q1mx * q3 + _2q1my * q2 + mz * q1q1 + _2q2mx * q4 - mz * q2q2 + _2q3 * my * q4 - mz * q3q3 + mz * q4q4;
    _4bx = 2.0f * _2bx;
    _4bz = 2.0f * _2bz;

    // Gradient decent algorithm corrective step
    s1 = -_2q3 * (2.0f * q2q4 - _2q1q3 - ax) + _2q2 * (2.0f * q1q2 + _2q3q4 - ay) - _2bz * q3 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q4 + _2bz * q2) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q3 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
    s2 = _2q4 * (2.0f * q2q4 - _2q1q3 - ax) + _2q1 * (2.0f * q1q2 + _2q3q4 - ay) - 4.0f * q2 * (1.0f - 2.0f * q2q2 - 2.0f * q3q3 - az) + _2bz * q4 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q3 + _2bz * q1) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q4 - _4bz * q2) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
    s3 = -_2q1 * (2.0f * q2q4 - _2q1q3 - ax) + _2q4 * (2.0f * q1q2 + _2q3q4 - ay) - 4.0f * q3 * (1.0f - 2.0f * q2q2 - 2.0f * q3q3 - az) + (-_4bx * q3 - _2bz * q1) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q2 + _2bz * q4) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q1 - _4bz * q3) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
    s4 = _2q2 * (2.0f * q2q4 - _2q1q3 - ax) + _2q3 * (2.0f * q1q2 + _2q3q4 - ay) + (-_4bx * q4 + _2bz * q2) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q1 + _2bz * q3) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q2 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
    norm = sqrtf(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4);    // normalise step magnitude
    norm = 1.0f/norm;
    s1 *= norm;
    s2 *= norm;
    s3 *= norm;
    s4 *= norm;

    // Compute rate of change of quaternion
    qDot1 = 0.5f * (-q2 * gx - q3 * gy - q4 * gz) - beta * s1;
    qDot2 = 0.5f * (q1 * gx + q3 * gz - q4 * gy) - beta * s2;
    qDot3 = 0.5f * (q1 * gy - q2 * gz + q4 * gx) - beta * s3;
    qDot4 = 0.5f * (q1 * gz + q2 * gy - q3 * gx) - beta * s4;

    // Integrate to yield quaternion
    q1 += qDot1 * deltat;
    q2 += qDot2 * deltat;
    q3 += qDot3 * deltat;
    q4 += qDot4 * deltat;
    norm = sqrtf(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalise quaternion
    norm = 1.0f/norm;
    q.w = q1 * norm;
    q.x = q2 * norm;
    q.y = q3 * norm;
    q.z = q4 * norm;
}

Quaternion qfa(float roll, float pitch, float yaw){
    Quaternion ret;
    float cy = cos(yaw * 0.5);
    float sy = sin(yaw * 0.5);
    float cp = cos(pitch * 0.5);
    float sp = sin(pitch * 0.5);
    float cr = cos(roll * 0.5);
    float sr = sin(roll * 0.5);

    ret.w = cy * cp * cr + sy * sp * sr;
    ret.x = cy * cp * sr - sy * sp * cr;
    ret.y = sy * cp * sr + cy * sp * cr;
    ret.z = sy * cp * cr - cy * sp * sr;

    return ret;
}

Vector afq(Quaternion* q) {
  Vector v;
    v.z = atan2(2*q -> x*q -> y - 2*q -> w*q -> z, 2*q -> w*q -> w + 2*q -> x*q -> x - 1);   // psi
    v.y = -asin(2*q -> x*q -> z + 2*q -> w*q -> y);                              // theta
    v.x = atan2(2*q -> y*q -> z - 2*q -> w*q -> x, 2*q -> w*q -> w + 2*q -> z*q -> z - 1);   // phi
    return v;
}

Vector afv(Vector v){
  Vector vv(0,0,0);
    vv.y = -asin(v.z);
    vv.z = atan2(v.y, v.x);
    return vv;
}

void printQuaternion(Quaternion q, bool end){
    Serial.print(q.w, 5);
    Serial.print(",");
    Serial.print(q.x, 5);
    Serial.print(",");
    Serial.print(q.y, 5);
    Serial.print(",");
    Serial.print(q.z, 5);
    if (end){
      Serial.println();
    } else{
      Serial.print(";  ");
    }
}