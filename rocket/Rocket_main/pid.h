#pragma once

#include <Arduino.h>
#include "pidClass.h"
#include "mathFunctions.h"

class pid{
    private:
      PID pidX;  
      PID pidY; 
      PID pidZ; 
      double SetpointX = 0, InputX, OutputX;
      double SetpointY = 0, InputY, OutputY;
      double SetpointZ = 0, InputZ, OutputZ;

      float derivativeFilterGain = 0.4;
      float derivativeFilterCutoff = 40; //Hz
      float X_kp = 12, X_ki = 90, X_kd = 0.65; // pid coefficients roll
      float Y_kp = 12, Y_ki = 90, Y_kd = 0.65; // pid coefficients pitch
      float Z_kp = 7, Z_ki = 50, Z_kd = 0.33; // pid coefficients yaw

    public:
      pid();
      void init(uint32_t loopUpdateRate);
      void updatePIDinput(Vector error);
      Vector getPIDoutput();
      void tuneUsingSerial();
      void getPIDcomponents(Vector &pitch, Vector &roll, Vector &yaw);
      void resetIntegral();
};