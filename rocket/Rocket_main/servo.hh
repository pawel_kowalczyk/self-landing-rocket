#pragma once

#include <PWMServo.h>

class Servo{
    private:
        PWMServo servo;
        int pin;
        float center;
        float calibration;
        float angle;
        const int max_angle = 35;
    protected:

    public:
        Servo();
        void init(int pin, float center, float calib);
        void write(float value);
        float mapfloat(float x, float in_min, float in_max, float out_min, float out_max);
};
