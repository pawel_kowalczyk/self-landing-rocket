import tkinter as tk
from tkinter import ttk
import pathlib
import pandas as pd
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from base_station_window import BaseStationApp

class RocketPathSelectorApp:
    def __init__(self, root, base_station_window):
        self.root = root
        self.base_station_window: "BaseStationApp" = base_station_window

        self.root.title("Rocket Path Selector")
        self.root.geometry("480x640")

        # Adjust font size and padding
        self.default_font = ("Arial", 12)
        self.large_font = ("Arial", 16)
        self.default_padding = 20
        self.large_padding = 30

        # Style configuration
        self.style = ttk.Style()
        self.style.configure('Large.TButton', font=self.large_font)
        self.style.configure('Default.TButton', font=self.default_font)

        # Button to load path
        self.load_path_button = ttk.Button(self.root, text="Load path", command=self.show_path_options, style='Large.TButton')
        self.load_path_button.pack(pady=self.large_padding)

        # Roll-down to display available paths
        self.path_combobox = ttk.Combobox(self.root, state="readonly", font=self.default_font)
        self.path_combobox.pack(pady=self.default_padding)
        self.path_combobox.bind("<<ComboboxSelected>>", self.show_path_description)

        # Accept button
        self.accept_button = ttk.Button(self.root, text="Accept", command=self.accept_path, style='Large.TButton')
        self.accept_button.pack_forget()  # Initially hidden

        # Description label
        self.description_label = ttk.Label(self.root, text="", font=self.default_font)
        self.description_label.pack(pady=self.default_padding)
        self.description_label.pack_forget()  # Initially hidden

        # Frames for main window and path creator
        self.main_frame = tk.Frame(self.root)
        self.path_creator_frame = tk.Frame(self.root)

    def load_paths(self):
        self.paths_folder = pathlib.Path(__file__).parent / "rocket_paths"
        paths = [path.stem for path in self.paths_folder.iterdir() if path.suffix == ".csv"]
        return paths    
    
    def show_path_options(self):
        # Assume available paths are fetched from somewhere and stored in a list
        available_paths = self.load_paths()
        self.path_combobox["values"] = available_paths
        self.path_combobox.current(0)  # Select the first option by default
        self.accept_button.pack(pady=self.large_padding)  # Show the accept button

    def show_path_description(self, event):
        selected_path = self.path_combobox.get()
        # Assume description for the selected path is fetched from somewhere
        self.points = pd.read_csv(pathlib.Path(__file__).parent / "rocket_paths" / f"{selected_path}.csv", sep=',', header=None).values
        description = f"{self.points}"
        self.description_label.config(text=description)
        self.description_label.pack()  # Show the description

    def accept_path(self):
        self.base_station_window.load_rocket_path(setpoints=self.points, name=self.path_combobox.get())
        self.root.destroy()
