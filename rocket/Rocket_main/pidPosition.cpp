#include "pidPosition.h"

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max);
float applyDeadband(float val, float deadband);

pidPosition::pidPosition():
  pidX(&InputX, &OutputX, &SetpointX, 0, 0, 0, DIRECT),
  pidY(&InputY, &OutputY, &SetpointY, 0, 0, 0, DIRECT),
  pidZ(&InputZ, &OutputZ, &SetpointZ, 0, 0, 0, DIRECT) {}

void pidPosition::init(uint32_t loopUpdateRate){
  pidX.SetOutputLimits(-maxBankingAngle, maxBankingAngle); //degrees of tilt 
  pidY.SetOutputLimits(-maxBankingAngle, maxBankingAngle); //degrees of tilt
  pidZ.SetOutputLimits(-maxThrottleDiff, maxThrottleDiff);
  pidX.SetSampleTime(1);
  pidY.SetSampleTime(1);
  pidZ.SetSampleTime(1);
  pidX.SetMode(AUTOMATIC);
  pidY.SetMode(AUTOMATIC);
  pidZ.SetMode(AUTOMATIC);
  pidX.SetTunings(X_kp, X_ki, X_kd);
  pidY.SetTunings(Y_kp, Y_ki, Y_kd);
  pidZ.SetTunings(Z_kp, Z_ki, Z_kd);
  pidX.init_DerivativeFilterCompl(derivativeFilterGain);
  pidY.init_DerivativeFilterCompl(derivativeFilterGain);
  pidZ.init_DerivativeFilterCompl(derivativeFilterGain);
}

void pidPosition::updateInput(Vector in){
  InputX = in.x;
  InputY = in.y;
  InputZ = in.z;
}

void pidPosition::setSetpoint(Vector set){
  SetpointX = set.x;
  SetpointY = set.y;
  SetpointZ = set.z;
}

void pidPosition::integrateSetpoint(Vector diff){
  SetpointX += applyDeadband(diff.x, 0.2)/1000;
  SetpointY += applyDeadband(diff.y, 0.2)/1000;
  SetpointZ += applyDeadband(diff.z, 0.5)/5000;
}

float applyDeadband(float val, float deadband){
  float result = 0;

  if (abs(val) > deadband){
    result = mapfloat(abs(val), deadband, 1, 0, 1);
    result *= abs(val) / val;
  }
  return result;
}

Vector pidPosition::getPIDoutput(){
  pidX.Compute();
  pidY.Compute();
  pidZ.Compute();
  Vector out(OutputX, OutputY, OutputZ);
  return out;
}

void pidPosition::getPIDcomponents(Vector &pitch, Vector &roll, Vector &yaw){
  pitch.x = pidY.get_proportional();
  pitch.y = pidY.get_integral();
  pitch.z = pidY.get_derivative();

  roll.x = pidX.get_proportional();
  roll.y = pidX.get_integral();
  roll.z = pidX.get_derivative();

  yaw.x = pidZ.get_proportional();
  yaw.y = pidZ.get_integral();
  yaw.z = pidZ.get_derivative();
}

void pidPosition::resetIntegral(){
  pidY.resetIntegral();
  pidX.resetIntegral();
  pidZ.resetIntegral();
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max){
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}