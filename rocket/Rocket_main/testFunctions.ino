void manualOperationWithTransmitter(){
  rx.getRX(channels);
  roll = channels[0]; pitch = channels[1]; throttle = channels[2]; yaw = channels[3];

  // Serial.print(roll); Serial.print(" ");
  // Serial.print(pitch); Serial.print(" ");
  // Serial.print(throttle); Serial.print(" ");
  // Serial.print(yaw); Serial.println(" ");

  if (!rx.isFailsafe() && rx.isArmed()){
    finB.write((roll + yaw)/2);
    finF.write((-roll + yaw)/2);
    finR.write((pitch + yaw)/2);
    finL.write((-pitch + yaw)/2);
    thruster.write(throttle);
  } else{
    finB.write(0);
    finF.write(0);
    finR.write(0);
    finL.write(0);
    thruster.write(-1);
  }
}