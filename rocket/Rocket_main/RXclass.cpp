#include "RXclass.h"
#include <Arduino.h>

RXclass::RXclass() : sbus_rx(&Serial3) {}

void RXclass::init(){
  sbus_rx.Begin();
  filterX.init_complementaryFilter(filterGain);
  filterY.init_complementaryFilter(filterGain);
  filterZ.init_complementaryFilter(filterGain);
}

bool RXclass::update(){
  bool newData = sbus_rx.Read();
  data = sbus_rx.data();
  return newData;
}

void RXclass::getRX(float (&mapped_channels)[8]){
  update();

  for (int8_t i = 0; i < 8; i++) {
    mapped_channels[i] = getRX(i);
  }
}

float RXclass::getRX(int i){
  update();
  float val  = mapfloat(data.ch[i], inputMin, inputMax, outputMin, outputMax);

  if (i == 0){
    val = filterX.process_complementaryFilter(val);
    val = applyDeadband(val);
    val = applyCurve(val);  
  } else if (i == 1){
    val = filterY.process_complementaryFilter(val);
    val = applyDeadband(val);
    val = applyCurve(val); 
  } else if (i == 3){
    val = filterZ.process_complementaryFilter(val);
    val = applyDeadband(val);
    val = applyCurve(val); 
  }
  
  return val;
}

float RXclass::applyCurve(float val){
  float result = val;

  if (curveExpo < 1 && curveExpo > 0){
    result = abs(val) * pow(val,5) * curveExpo + val * (1 - curveExpo);
  }
  return result;
}

float RXclass::applyDeadband(float val){
  float result = 0;

  if (abs(val) > deadband){
    result = mapfloat(abs(val), deadband, outputMax, 0, outputMax);
    result *= abs(val) / val;
  }
  return result;
}

flightModes RXclass::getFlightMode(){
  float val = getRX(6);
  if (val < -0.5){ return Angle; }  // self-levelling mode
  if (val > 0.5){ return Position; }   // postion mode using camera
  return Angle;
}

legsModes RXclass::getLegsMode(){
  float val = getRX(5);
  if (val < -0.5){ return Retracted; }  // self-levelling mode
  if (val > -0.5 && val < 0.5){ return Extended; } // self-levelling mode
  if (val > 0.5){ return ExtendedWithForce; }   // postion mode using camera
  return Retracted;
}

bool RXclass::isFailsafe(){
  update();
  return data.failsafe || data.ch[0]<50 || data.ch[1]<50;
}

bool RXclass::isArmed(){
  update();
  if (isThrottleZero() && getRX(4)>0){
    armed = true;
  } else if (getRX(4)<0){
    armed = false;
  }
  return armed;
}

bool RXclass::isThrottleZero(){
  update();
  return getRX(2) < -0.97;
}

float RXclass::mapfloat(float x, float in_min, float in_max, float out_min, float out_max){
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

